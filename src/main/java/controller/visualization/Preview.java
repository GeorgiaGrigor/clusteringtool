package controller.visualization;

import controller.visualization.plugins.preview.PreviewSketch;
import org.gephi.graph.api.Node;
import org.gephi.preview.api.*;
import org.gephi.preview.types.DependantOriginalColor;
import org.gephi.preview.types.EdgeColor;
import org.openide.util.Lookup;
import controller.Project;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * @author Georgia Grigoriadou
 */

public class Preview {

    private static G2DTarget         target;
    private static PreviewController previewController;
    private static PreviewModel      previewModel;
    private static PreviewSketch     previewSketch;

    public static DependantOriginalColor labelColor;
    public static Font                   labelFont;
    public static float                  labelSize = 0;
    public static float                  edgeThickness;
    public static boolean                showEdges;
    public static boolean                curveEdges;
    public static boolean                showLabels;

    public static JPanel getPreviewPanel() {
        previewController = Lookup.getDefault().lookup(PreviewController.class);
        previewModel      = previewController.getModel();

        if (Project.getFileProperties()== null || !Project.getFileProperties().getLocation().endsWith(".controller.visualization")) {
            previewModel.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
            previewModel.getProperties().putValue(PreviewProperty.EDGE_CURVED, Boolean.FALSE);
            previewModel.getProperties().putValue(PreviewProperty.EDGE_OPACITY, 50);
            previewModel.getProperties().putValue(PreviewProperty.BACKGROUND_COLOR, Color.WHITE);
        }

        target        = (G2DTarget) previewController.getRenderTarget(RenderTarget.G2D_TARGET);
        previewSketch = new PreviewSketch(target);
        previewController.refreshPreview();

        initProperties();

        //Add the applet to a JFrame and display
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(previewSketch, BorderLayout.CENTER);

        //Wait for the frame to be visible before painting, or the result drawing will be strange
            panel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {previewSketch.resetZoom();}

            @Override
            public void componentShown(ComponentEvent e) {previewSketch.resetZoom();}

        });

        return panel;
    }

    public static void setBackgroundColor(){
        if(previewModel.getProperties().getColorValue(PreviewProperty.BACKGROUND_COLOR) == Color.WHITE) {
            previewModel.getProperties().putValue(PreviewProperty.BACKGROUND_COLOR, Color.BLACK);
            if(((DependantOriginalColor)previewModel.getProperties().getValue(PreviewProperty.NODE_LABEL_COLOR)).getCustomColor() == Color.BLACK) {
                previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_COLOR, new DependantOriginalColor(Color.WHITE));
            }
        } else {
            previewModel.getProperties().putValue(PreviewProperty.BACKGROUND_COLOR, Color.WHITE);
            if(((DependantOriginalColor)previewModel.getProperties().getValue(PreviewProperty.NODE_LABEL_COLOR)).getCustomColor() ==  Color.WHITE) {
                previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_COLOR, new DependantOriginalColor(Color.BLACK));
            }
        }

        refreshPreview();
    }

    public static void setBackgroundColor(Color color){
        previewModel.getProperties().putValue(PreviewProperty.BACKGROUND_COLOR, color);

        refreshPreview();
    }

    public static void setLabelColor(DependantOriginalColor color){
        previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_COLOR, color);
        labelColor = color.getMode() ==  DependantOriginalColor.Mode.CUSTOM ? color:labelColor;

        refreshPreview();
    }

    public static void setEdgeColor(EdgeColor color){
        previewModel.getProperties().putValue(PreviewProperty.EDGE_COLOR, color);

        refreshPreview();
    }

    public static void setBooleanProperty(String previewProperty, boolean value){
        previewModel.getProperties().putValue(previewProperty, value);

        refreshPreview();
    }

    public static void setEdgeThickness(float size) {
        previewModel.getProperties().putValue(PreviewProperty.EDGE_THICKNESS, size);

        refreshPreview();
    }

    public static void setLabelFont(Font font) {
        previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_FONT, font);
        previewController.refreshPreview();
        labelFont = font;

        refreshPreview();

    }

    private static void initProperties(){
        labelColor    = previewModel.getProperties().getValue(PreviewProperty.NODE_LABEL_COLOR);
        edgeThickness = previewModel.getProperties().getFloatValue(PreviewProperty.EDGE_THICKNESS);
        labelFont     = previewModel.getProperties().getFontValue(PreviewProperty.NODE_LABEL_FONT);
        showEdges     = previewModel.getProperties().getBooleanValue(PreviewProperty.SHOW_EDGES);
        curveEdges    = previewModel.getProperties().getBooleanValue(PreviewProperty.EDGE_CURVED);
        showLabels    = previewModel.getProperties().getBooleanValue(PreviewProperty.SHOW_NODE_LABELS);
        for (Node node : Project.getGraphModel().getDirectedGraph().getNodes()) {
            labelSize = node.getTextProperties().getSize() > labelSize ? node.getTextProperties().getSize() : labelSize;
        }
    }

    public static void refreshPreview() {
        if (previewController != null) {
            previewController.refreshPreview();

            target.refresh();
            previewSketch.repaint();
        }
    }

    public static void resetZoom(){
        previewSketch.resetZoom();
    }

    public static void reset(){
        previewController = null;
        previewModel      = null;
        target            = null;
        previewSketch     = null;
    }

}
