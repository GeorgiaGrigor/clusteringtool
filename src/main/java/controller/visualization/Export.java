package controller.visualization;

import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.exporter.spi.GraphExporter;
import org.openide.util.Lookup;
import controller.Project;

import java.io.File;
import java.io.IOException;

/**
 * @author Georgia Grigoriadou
 */

public class Export {
    private static ExportController exportController = Lookup.getDefault().lookup(ExportController.class);


    public static void exportAsGraph(String path, String type, boolean visibleOnly) {
        //Export only visible graph
        if (visibleOnly) {
            GraphExporter exporter = (GraphExporter) exportController.getExporter(type);
            exporter.setExportVisible(true);
            exporter.setWorkspace(Project.getWorkspace());

            try {
                exportController.exportFile(new File(path + type), exporter);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            try {
                exportController.exportFile(new File(path));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public static void exportAsImage(String path) {
        try {
            exportController.exportFile(new File(path));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
