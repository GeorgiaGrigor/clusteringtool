package controller.visualization;

import controller.Project;
import model.api.Cluster;
import org.gephi.appearance.api.AppearanceController;
import org.gephi.appearance.api.AppearanceModel;
import org.gephi.filters.api.FilterController;
import org.gephi.filters.api.Query;
import org.gephi.filters.api.Range;
import org.gephi.filters.plugin.graph.DegreeRangeBuilder;
import org.gephi.filters.plugin.graph.InDegreeRangeBuilder;
import org.gephi.filters.plugin.graph.OutDegreeRangeBuilder;
import org.gephi.filters.plugin.operator.INTERSECTIONBuilder;
import org.gephi.filters.plugin.operator.UNIONBuilder;
import org.gephi.filters.plugin.partition.InterEdgesBuilder;
import org.gephi.filters.plugin.partition.IntraEdgesBuilder;
import org.gephi.filters.plugin.partition.PartitionBuilder;
import org.gephi.filters.spi.FilterBuilder;
import org.gephi.graph.api.Column;
import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.GraphView;
import org.gephi.graph.impl.EdgeImpl;
import org.gephi.graph.impl.GraphViewImpl;
import org.gephi.graph.impl.NodeImpl;
import org.openide.util.Lookup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static controller.visualization.GephiInitializer.CLUSTER;
import static controller.visualization.Preview.refreshPreview;

/**
 * @author Gewrgia
 */

public class Filter {
    private static FilterController filterController = Lookup.getDefault().lookup(FilterController.class);
    private static Query nodeQuery;
    private static Query degreeQuery;
    private static Query inDegreeQuery;
    private static Query outDegreeQuery;
    private static Query intraQuery;
    private static Query interQuery;
    private static List<String> intraClusters = new ArrayList<>();
    private static List<String> interClusters = new ArrayList<>();

    private static boolean filterClustersBySize = false;
    private static boolean filterClustersByFactor = false;
    private static boolean showNeighbours = false;
    private static boolean hideNodes = false;

    private static Range clusterSizeRange;
    private static Range clusterFactorRange;


    public static void createNodeQuery(ArrayList<String> clusters) {
        PartitionBuilder builder = new PartitionBuilder();
        FilterBuilder[] builders = builder.getBuilders(Project.getWorkspace());
        Column column = Project.getGraphModel().getNodeTable().getColumn(CLUSTER);

        PartitionBuilder.NodePartitionFilter filter = null;
        if (builders.length < 1) {
            AppearanceController appearanceController = Lookup.getDefault().lookup(AppearanceController.class);
            AppearanceModel appearanceModel = appearanceController.getModel();
            filter = new PartitionBuilder.NodePartitionFilter(column, appearanceModel);
        } else {
            for (FilterBuilder filterBuilder : builders) {
                try {
                    filter = (PartitionBuilder.NodePartitionFilter) filterBuilder.getFilter(Project.getWorkspace());
                    if (filter.getColumn() == column) {
                        break;
                    }
                } catch (Exception ignored) {
                }
            }
        }
        if (filter != null) {
            filter.unselectAll();
            clusters
                    .stream()
                    .filter(p ->
                            new ClusterFilter(
                                    (cluster) ->
                                            (!filterClustersBySize || ClusterFilter.clusterSizeFilter.test(cluster, clusterSizeRange))
                                                    && (!filterClustersByFactor || ClusterFilter.clusterFactorFilter.test(cluster, clusterFactorRange))
                            )
                                    .test(p))
                    .forEach(filter::addPart);
            nodeQuery = filterController.createQuery(filter);
        }
    }

    public static void setClusterSizeFilter(boolean filter, int min, int max) {
        filterClustersBySize = filter;
        clusterSizeRange = filterClustersBySize ? new Range(min, max) : null;
    }

    public static void setClusterFactorFilter(boolean filter, float min, float max) {
        filterClustersByFactor = filter;
        clusterFactorRange     = filterClustersByFactor ? new Range(min, max) : null;
    }

    public static void createDegreeQuery(int min, int max) {
        DegreeRangeBuilder.DegreeRangeFilter filter = new DegreeRangeBuilder.DegreeRangeFilter();
        filter.init(Project.getGraphModel().getGraph());
        filter.setRange(new Range(min, max));
        degreeQuery = filterController.createQuery(filter);
    }

    public static void createInDegreeQuery(int min, int max) {
        InDegreeRangeBuilder.InDegreeRangeFilter filter = new InDegreeRangeBuilder.InDegreeRangeFilter();
        filter.init(Project.getGraphModel().getGraph());
        filter.setRange(new Range(min, max));
        inDegreeQuery = filterController.createQuery(filter);
    }

    public static void createOutDegreeQuery(int min, int max) {
        OutDegreeRangeBuilder.OutDegreeRangeFilter filter = new OutDegreeRangeBuilder.OutDegreeRangeFilter();
        filter.init(Project.getGraphModel().getGraph());
        filter.setRange(new Range(min, max));
        outDegreeQuery = filterController.createQuery(filter);
    }

    public static void createIntraEdgeQuery(ArrayList<String> intra) {
        IntraEdgesBuilder builder = new IntraEdgesBuilder();
        FilterBuilder[] builders = builder.getBuilders(Project.getWorkspace());
        Column column = Project.getGraphModel().getNodeTable().getColumn(CLUSTER);

        IntraEdgesBuilder.IntraEdgesFilter filter = null;
        if (builders.length < 1) {
            AppearanceController appearanceController = Lookup.getDefault().lookup(AppearanceController.class);
            AppearanceModel appearanceModel = appearanceController.getModel();
            filter = new IntraEdgesBuilder.IntraEdgesFilter(column, appearanceModel);
        } else {
            for (FilterBuilder filterBuilder : builders) {
                try {
                    filter = (IntraEdgesBuilder.IntraEdgesFilter) filterBuilder.getFilter(Project.getWorkspace());
                    if (filter.getColumn() == column) {
                        break;
                    }
                } catch (Exception ignored) {
                }
            }
        }
        if (filter != null) {
            filter.unselectAll();
            intra.forEach(filter::addPart);
            intraQuery    = filterController.createQuery(filter);
            intraClusters = intra;
        }
    }

    public static void createInterEdgeQuery(ArrayList<String> inter) {
        InterEdgesBuilder builder = new InterEdgesBuilder();
        FilterBuilder[] builders = builder.getBuilders(Project.getWorkspace());
        Column column = Project.getGraphModel().getNodeTable().getColumn(CLUSTER);

        InterEdgesBuilder.InterEdgesFilter filter = null;
        if (builders.length < 1) {
            AppearanceController appearanceController = Lookup.getDefault().lookup(AppearanceController.class);
            AppearanceModel appearanceModel = appearanceController.getModel();
            filter = new InterEdgesBuilder.InterEdgesFilter(column, appearanceModel);
        } else {

            for (FilterBuilder filterBuilder : builders) {
                try {
                    filter = (InterEdgesBuilder.InterEdgesFilter) filterBuilder.getFilter(Project.getWorkspace());
                    if (filter.getColumn() == column) {
                        break;
                    }
                } catch (Exception ignored) {
                }
            }
        }
        if (filter != null) {
            filter.unselectAll();
            inter.forEach(filter::addPart);
            interQuery    = filterController.createQuery(filter);
            interClusters = inter;
        }
    }

    public static void setShowNeighbours(boolean showNeighbours) {
        Filter.showNeighbours = showNeighbours;
    }

    public static void setHideNodes(boolean hideNodes) {
        Filter.hideNodes = hideNodes;
    }

    public static void filter() {
        DirectedGraph directedGraph = Project.getGraphModel().getDirectedGraph();
        GraphModel graphModel = Project.getGraphModel();
        graphModel.getGraph().readUnlockAll();
        graphModel.setVisibleView(directedGraph.getView());
        GraphView view = directedGraph.getView();

        UNIONBuilder.UnionOperator unionOperator = new UNIONBuilder.UnionOperator();
        INTERSECTIONBuilder.IntersectionOperator intersectionOperator = new INTERSECTIONBuilder.IntersectionOperator();
        Query query  = filterController.createQuery(intersectionOperator);
        Query nQuery = null;
        Query eQuery = null;

        if (nodeQuery != null || degreeQuery != null || inDegreeQuery != null || outDegreeQuery != null) {
            nQuery = filterController.createQuery(intersectionOperator);
            if (nodeQuery != null) filterController.setSubQuery(nQuery, nodeQuery);
            if (degreeQuery != null) filterController.setSubQuery(nQuery, degreeQuery);
            if (inDegreeQuery != null) filterController.setSubQuery(nQuery, inDegreeQuery);
            if (outDegreeQuery != null) filterController.setSubQuery(nQuery, outDegreeQuery);
            view = filterController.filter(nQuery);
            filterController.setSubQuery(query, nQuery);
        }
        if (intraQuery != null || interQuery != null) {
            eQuery = filterController.createQuery(unionOperator);
            if (intraQuery != null) filterController.setSubQuery(eQuery, intraQuery);
            if (interQuery != null) filterController.setSubQuery(eQuery, interQuery);

            filterController.setSubQuery(query, eQuery);
        }
        if (nQuery != null || eQuery != null) {
            view = filterController.filter(query);
            graphModel.setVisibleView(view);
        }

        if (showNeighbours) {
            showNeighbours(view);
/*          !!!IGNORES EDGES FILTERS!!!
            Query queryEgo = filterController.createQuery(unionOperator);
            for (Node node : graphModel.getDirectedGraphVisible().getNodes()) {

                EgoBuilder.EgoFilter egoFilter = new EgoBuilder.EgoFilter();
                egoFilter.setPattern(node.getId().toString());
                egoFilter.setDepth(1);
                Query q = filterController.createQuery(egoFilter);
                filterController.setSubQuery(queryEgo, q);

            }
            view = filterController.filter(queryEgo);*/
        }
        if (hideNodes) {
            hideNodes(view);
        }

        graphModel.getGraph().readUnlockAll();
        graphModel.setVisibleView(view);
        refreshPreview();
    }

    public static void reset() {
        nodeQuery      = null;
        degreeQuery    = null;
        inDegreeQuery  = null;
        outDegreeQuery = null;
        intraQuery     = null;
        interQuery     = null;

        filterClustersBySize   = false;
        filterClustersByFactor = false;
        showNeighbours         = false;
        hideNodes              = false;

        clusterSizeRange   = null;
        clusterFactorRange = null;

        GraphModel graphModel = Project.getGraphModel();
        if (graphModel != null) {
            graphModel.getGraph().readUnlockAll();
            graphModel.setVisibleView(graphModel.getDirectedGraph().getView());
            refreshPreview();
        }
    }

    private static void showNeighbours(GraphView view) {
        GraphModel graphModel = Project.getGraphModel();

        DirectedGraph directedGraph = Project.getGraphModel().getDirectedGraph();
        GraphViewImpl viewImpl = (GraphViewImpl) view;
        Set<org.gephi.graph.api.Node> nodes = new HashSet<>();
        graphModel.getDirectedGraphVisible().getNodes().forEach(node -> directedGraph.getNeighbors(node).forEach(nodes::add));
        nodes.forEach(viewImpl::addNode);

        graphModel.getDirectedGraphVisible().getNodes().forEach(
                node -> directedGraph.getEdges(node).forEach(edge -> {
                    if (!viewImpl.containsEdge((EdgeImpl) edge) && viewImpl.containsNode((NodeImpl) edge.getSource()) && viewImpl.containsNode((NodeImpl) edge.getTarget())) {
                        if (intraQuery == null && interQuery == null) {
                            viewImpl.addEdge(edge);
                        } else {
                            String sCluster = (String) edge.getSource().getAttribute(GephiInitializer.CLUSTER);
                            String tCluster = (String) edge.getTarget().getAttribute(GephiInitializer.CLUSTER);
                            boolean intraEdge = false;
                            boolean interEdge = false;
                            if (intraQuery != null && interQuery != null) {
                                if (sCluster.equals(tCluster)) {
                                    intraEdge = true;
                                } else {
                                    interEdge = true;
                                }
                            }
                            if (intraQuery != null && intraEdge) {
                                if (intraClusters.contains(sCluster)) {
                                    viewImpl.addEdge(edge);
                                }
                            } else if (interQuery != null && interEdge) {
                                if (interClusters.contains(sCluster) && interClusters.contains(tCluster)) {
                                    viewImpl.addEdge(edge);
                                }
                            }
                        }
                    }
                }));
    }

    private static void hideNodes(GraphView view) {
        GraphModel graphModel = Project.getGraphModel();
        GraphViewImpl viewImpl = (GraphViewImpl) view;
        graphModel.getDirectedGraphVisible().getNodes().forEach(
                node -> {
                    if (graphModel.getDirectedGraphVisible().getEdges(node).toCollection().size() == 0) {
                        viewImpl.removeNode(node);
                    }
                });
    }

    private static class ClusterFilter implements Predicate<String> {
        private Predicate<Cluster> predicate;

        private ClusterFilter(Predicate<Cluster> predicate) {
            this.predicate = predicate;
        }

        @Override
        public boolean test(String s) {
            AtomicBoolean result = new AtomicBoolean(true);
            Project.getMdg().getClusters()
                    .stream()
                    .filter(cluster -> cluster.getId().equals(s))
                    .forEach(
                            cluster -> result.set(predicate.test(cluster)));
            return result.get();
        }

        private static BiPredicate<Cluster, Range> clusterSizeFilter = (cluster, range) ->
                range.getLowerInteger() <= cluster.getNodes().size() &&
                        range.getUpperInteger() >= cluster.getNodes().size();

        private static BiPredicate<Cluster, Range> clusterFactorFilter = (cluster, range) ->
                range.getLowerFloat() <= cluster.getClusterFactor() &&
                        range.getUpperFloat() >= cluster.getClusterFactor();
    }
}
