package controller.visualization;

import controller.visualization.plugins.layout.ClusterLayout;
import controller.visualization.plugins.layout.ClusterLayoutBuilder;
import org.gephi.graph.api.TextProperties;
import org.gephi.layout.plugin.force.yifanHu.YifanHu;
import org.gephi.layout.plugin.force.yifanHu.YifanHuLayout;
import org.gephi.layout.plugin.force.yifanHu.YifanHuProportional;
import org.gephi.layout.plugin.forceAtlas.ForceAtlas;
import org.gephi.layout.plugin.forceAtlas.ForceAtlasLayout;
import org.gephi.layout.plugin.forceAtlas2.ForceAtlas2;
import org.gephi.layout.plugin.forceAtlas2.ForceAtlas2Builder;
import org.gephi.layout.plugin.fruchterman.FruchtermanReingold;
import org.gephi.layout.plugin.fruchterman.FruchtermanReingoldBuilder;
import org.gephi.layout.plugin.labelAdjust.LabelAdjust;
import org.gephi.layout.plugin.labelAdjust.LabelAdjustBuilder;
import org.gephi.layout.plugin.noverlap.NoverlapLayout;
import org.gephi.layout.plugin.noverlap.NoverlapLayoutBuilder;
import org.gephi.layout.plugin.openord.OpenOrdLayout;
import org.gephi.layout.plugin.openord.OpenOrdLayoutBuilder;
import org.gephi.layout.plugin.random.Random;
import org.gephi.layout.plugin.random.RandomLayout;
import org.gephi.layout.plugin.rotate.Rotate;
import org.gephi.layout.plugin.rotate.RotateLayout;
import org.gephi.layout.plugin.scale.Contract;
import org.gephi.layout.plugin.scale.Expand;
import org.gephi.layout.plugin.scale.ScaleLayout;
import controller.Project;

/**
 * @author Georgia Grigoriadou
 */

public class Layout {

    public static ClusterLayout getClusterLayout(Double margin){
        ClusterLayoutBuilder builder = new ClusterLayoutBuilder();
        ClusterLayout layout = new ClusterLayout(builder);
        layout.resetPropertiesValues();
        layout.setMargin(margin);
        layout.setGraphModel(Project.getGraphModel());
        layout.setWorkspace(Project.getWorkspace());
        layout.setColumn(Project.getGraphModel().getNodeTable().getColumn(GephiInitializer.CLUSTER));
        return layout;
    }

    public static ScaleLayout getContractLayout(double scaleFactor){
        Contract contract  = new Contract();
        ScaleLayout layout = contract.buildLayout();
        layout.setScale(scaleFactor);
        layout.setGraphModel(Project.getGraphModel());

        return layout;
    }

    public static ScaleLayout getExpansionLayout(double scaleFactor){
        Expand expand      = new Expand();
        ScaleLayout layout = expand.buildLayout();
        layout.setScale(scaleFactor);
        layout.setGraphModel(Project.getGraphModel());

       return layout;
    }

    public static  ForceAtlasLayout getForceAtlasLayout(double[] doubleValues, boolean[] booleanValues){
        ForceAtlas builder      = new ForceAtlas();
        ForceAtlasLayout layout = new ForceAtlasLayout(builder);
        layout.resetPropertiesValues();
        layout.setGraphModel(Project.getGraphModel());
        layout.setInertia(doubleValues[0]);
        layout.setRepulsionStrength(doubleValues[1]);
        layout.setAttractionStrength(doubleValues[2]);
        layout.setMaxDisplacement(doubleValues[3]);
        layout.setFreezeBalance(booleanValues[0]);
        layout.setFreezeStrength(doubleValues[4]);
        layout.setFreezeInertia(doubleValues[5]);
        layout.setGravity(doubleValues[6]);
        layout.setOutboundAttractionDistribution(booleanValues[1]);
        layout.setAdjustSizes(booleanValues[2]);
        layout.setSpeed(doubleValues[7]);

        return layout;
    }

    public static ForceAtlas2 getForceAtlas2Layout(int threads, double[] doubleValues, boolean[] booleanValues){
        ForceAtlas2Builder builder = new ForceAtlas2Builder();
        ForceAtlas2 layout         = new ForceAtlas2(builder);
        layout.resetPropertiesValues();
        layout.setGraphModel(Project.getGraphModel());
        layout.setThreadsCount(threads);
        layout.setJitterTolerance(doubleValues[0]);
        layout.setBarnesHutOptimize(booleanValues[0]);
        layout.setBarnesHutTheta(doubleValues[1]);
        layout.setScalingRatio(doubleValues[2]);
        layout.setStrongGravityMode(booleanValues[1]);
        layout.setGravity(doubleValues[3]);
        layout.setOutboundAttractionDistribution(booleanValues[2]);
        layout.setLinLogMode(booleanValues[3]);
        layout.setAdjustSizes(booleanValues[4]);
        layout.setEdgeWeightInfluence(doubleValues[4]);

        return layout;
    }

    public static FruchtermanReingold setFruchtermanReinGoldLayout(float area, double gravity, double speed){
        FruchtermanReingoldBuilder builder = new FruchtermanReingoldBuilder();
        FruchtermanReingold layout         = new FruchtermanReingold(builder);
        layout.setGraphModel(Project.getGraphModel());
        layout.setArea(area);
        layout.setGravity(gravity);
        layout.setSpeed(speed);

        return layout;
    }

    public static LabelAdjust getLabelAdjustLayout(double speed, boolean includeNodeSize){
        for (org.gephi.graph.api.Node n : Project.getGraphModel().getGraph().getNodes()) {
            TextProperties textProperties = n.getTextProperties();
            String label                  = n.getLabel();
            textProperties.setText(label);
            textProperties.setDimensions(label.length()*12, 30);
        }
        LabelAdjustBuilder builder = new LabelAdjustBuilder();
        LabelAdjust layout         = new LabelAdjust(builder);
        layout.setGraphModel(Project.getGraphModel());
        layout.setSpeed(speed);
        layout.setAdjustBySize(includeNodeSize);

        return layout;
    }

    public static NoverlapLayout getNonoverlapLayout(double speed, double ratio, double margin){
        NoverlapLayoutBuilder builder = new NoverlapLayoutBuilder();
        NoverlapLayout layout         = new NoverlapLayout(builder);
        layout.resetPropertiesValues();
        layout.setSpeed(speed);
        layout.setRatio(ratio);
        layout.setMargin(margin);
        layout.setGraphModel(Project.getGraphModel());

        return layout;
    }

    public static OpenOrdLayout getOpenOrdLayout(int[] intValues, float[] floatValues, long randomSeed){
        OpenOrdLayoutBuilder builder = new OpenOrdLayoutBuilder();
        OpenOrdLayout layout         = new OpenOrdLayout(builder);
        layout.setGraphModel(Project.getGraphModel());
        layout.resetPropertiesValues();
        layout.setLiquidStage(intValues[0]);
        layout.setExpansionStage(intValues[1]);
        layout.setCooldownStage(intValues[2]);
        layout.setCrunchStage(intValues[3]);
        layout.setSimmerStage(intValues[4]);
        layout.setEdgeCut(floatValues[0]);
        layout.setNumThreads(intValues[5]);
        layout.setNumIterations(intValues[6]);
        layout.setRealTime(floatValues[1]);
        layout.setRandSeed(randomSeed);

        return layout;
    }

    public static RandomLayout getRandomLayout(int size){
        Random builder      = new Random();
        RandomLayout layout = new RandomLayout(builder, size);
        layout.setGraphModel(Project.getGraphModel());

        return layout;
    }

    public static RotateLayout getRotateLayout(double angle){
        Rotate rotate       = new Rotate();
        RotateLayout layout = new RotateLayout(rotate, angle);
        layout.setGraphModel(Project.getGraphModel());

        return layout;
    }

    public static YifanHuLayout getYifanHuLayout(float[] floatValues, boolean adaptiveCooling, int quadTreeMaxLvl) {
        YifanHu builder      = new YifanHu();
        YifanHuLayout layout = builder.buildLayout();
        layout.setGraphModel(Project.getGraphModel());
        layout.resetPropertiesValues();
        layout.setOptimalDistance(floatValues[0]);
        layout.setRelativeStrength(floatValues[1]);
        layout.setInitialStep(floatValues[2]);
        layout.setStepRatio(floatValues[3]);
        layout.setAdaptiveCooling(adaptiveCooling);
        layout.setConvergenceThreshold(floatValues[4]);
        layout.setQuadTreeMaxLevel(quadTreeMaxLvl);
        layout.setBarnesHutTheta(floatValues[5]);

        return layout;
    }

    public static YifanHuLayout getYifanHuProportionalLayout(float[] floatValues, boolean adaptiveCooling, int quadTreeMaxLvl) {
        YifanHuProportional builder = new YifanHuProportional();
        YifanHuLayout layout        = builder.buildLayout();
        layout.setGraphModel(Project.getGraphModel());
        layout.resetPropertiesValues();
        layout.setOptimalDistance(floatValues[0]);
        layout.setRelativeStrength(floatValues[1]);
        layout.setInitialStep(floatValues[2]);
        layout.setStepRatio(floatValues[3]);
        layout.setAdaptiveCooling(adaptiveCooling);
        layout.setConvergenceThreshold(floatValues[4]);
        layout.setQuadTreeMaxLevel(quadTreeMaxLvl);
        layout.setBarnesHutTheta(floatValues[5]);

        return layout;
    }

    static void startLayout(org.gephi.layout.spi.Layout layout) {
        layout.initAlgo();
        while (layout.canAlgo()) {
            layout.goAlgo();
        }
        layout.endAlgo();
    }

}
