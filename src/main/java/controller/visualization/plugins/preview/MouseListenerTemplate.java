package controller.visualization.plugins.preview;

import controller.visualization.Preview;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.Node;
import org.gephi.preview.api.PreviewMouseEvent;
import org.gephi.preview.api.PreviewProperties;
import org.gephi.preview.spi.PreviewMouseListener;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

import javax.swing.*;

import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

/*
Copyright 2008-2014 Gephi
Authors : Eduardo Ramos <eduardo.ramos@controller.visualization.org>
Website : http://www.gephi.org

This file is part of Gephi.

Gephi is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Gephi is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Gephi.  If not, see <http://www.gnu.org/licenses/>.
 */

@SuppressWarnings("WeakerAccess")
@ServiceProvider(service=PreviewMouseListener.class)
public class MouseListenerTemplate implements PreviewMouseListener {
    private static boolean isMoving = false;
    private static Node clickedNode = null;

    @Override
    public void mouseClicked(PreviewMouseEvent event, PreviewProperties properties, Workspace workspace) {
//        if( event.button == PreviewMouseEvent.Button.LEFT) {
//            for (Node node : Lookup.getDefault().lookup(GraphController.class).getGraphModel(workspace).getGraph().getNodes()) {
//                if (clickingInNode(node, event)) {
//                    properties.putValue("display-label.node.id", node.getId());
//                    JOptionPane.showMessageDialog(null, "Node: " + node.getLabel() + " \nCluster: " + node.getAttribute("Cluster"), node.getLabel(), INFORMATION_MESSAGE);
//                     event.setConsumed(true);//So the renderer is executed and the graph repainted
//                    return;
//                }
//            }
//
//            properties.removeSimpleValue("display-label.node.id");
//            event.setConsumed(true);//So the renderer is executed and the graph repainted
//        }

    }

    @Override
    public void mousePressed(PreviewMouseEvent event, PreviewProperties properties, Workspace workspace) {

        for (Node node : Lookup.getDefault().lookup(GraphController.class).getGraphModel(workspace).getGraph().getNodes()) {
            if (clickingInNode(node, event)) {
                clickedNode = node;
                isMoving = true;
                return;
            }
        }
    }

    @Override
    public void mouseDragged(PreviewMouseEvent event, PreviewProperties properties, Workspace workspace) {
        if(isMoving){
            clickedNode.setX(event.x);
            clickedNode.setY(-event.y);
            Preview.refreshPreview();
        } else {
            throw new NullPointerException();
        }
    }

    @Override
    public void mouseReleased(PreviewMouseEvent event, PreviewProperties properties, Workspace workspace) {
        isMoving = false;
        clickedNode = null;
    }

    public static boolean clickingInNode(Node node, PreviewMouseEvent event) {
        float xdiff = node.x() - event.x;
        float ydiff = -node.y() - event.y;//Note that y axis is inverse for node coordinates
        float radius = node.size();

        return xdiff * xdiff + ydiff * ydiff < radius * radius;
    }

}
