package controller.clustering;

import model.TriFunction;
import model.api.Cluster;
import model.api.Graph;
import model.api.MDG;
import model.api.Node;

import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.ToIntFunction;

/**
 * @author Georgia Grigoriadou
 */

public class RandomClusterGenerator {

    public TriFunction<Graph, MDG, Cluster, MDG> generate = (graph, m, cluster) -> {
        MDG mdg = m.clone();
        int numClusters = configureNumberOfClusters.applyAsInt(graph);
        mdg             = createClusters.apply(mdg, cluster,numClusters);
        mdg             = fillClusters.apply(mdg, graph);
        mdg.deleteEmptyClusters();

        return mdg;
    };

    private static ToIntFunction<Graph> configureNumberOfClusters = (graph) -> {
        int numClusters = 0;
        while (numClusters <= 0) { numClusters = new Random().nextInt(graph.getNodes().size()); }
        return numClusters;
    };

    private static TriFunction<MDG, Cluster, Integer, MDG> createClusters = (mdg, cluster, numClusters) -> {
        for (int i = 1; i <= numClusters; i++) {
            Cluster cl = cluster.clone();
            mdg.addCluster(cl);
        }
        return mdg;
    };

    private static BiFunction<MDG, Graph, MDG> fillClusters = (mdg, graph) -> {
        int size = mdg.getClusters().size();
        for (Node node : graph.getNodes()) {
            if (!node.getNeighbours().isEmpty()) {
                int item = new Random().nextInt(size);
                int i    = 0;
                for (Cluster cluster : mdg.getClusters()) {
                    if (i == item)
                        mdg.addNodeToTargetCluster(cluster, node);
                    i++;
                }
            } else {
                mdg.getIndependentNodes().add(node);
            }
        }
        return mdg;
    };

}
