package controller.clustering;

import model.api.Cluster;
import model.api.Graph;
import model.api.MDG;

/**
 * @author Gewrgia
 */

interface ClusteringAlgorithm {

    MDG cluster(Graph graph, MDG mdg, Cluster cluster, int iterations);

}
