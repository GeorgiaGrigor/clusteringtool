package model;

/**
 * @author Georgia Grigoriadou
 */

@FunctionalInterface
public interface TriFunction<T, U, S, V> {

        V apply(T t, U u, S s);

}
