package model.impl;

import model.api.Cluster;
import model.api.MDG;
import model.api.Node;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Georgia Grigoriadou
 */

public class MDGImpl implements MDG {

    private Set<Node>    independentNodes;
    private Set<Cluster> clusters;
    private float        modularizationQuality;     // The sum of each CLUSTER's CF for the partitioned MDG

    public MDGImpl() {
        this.independentNodes      = new HashSet<>();
        this.clusters              = new HashSet<>();
        this.modularizationQuality = 0;
    }

    @Override
    public Set<Node> getIndependentNodes() {
        return independentNodes;
    }

    @Override
    public Set<Cluster> getClusters() {
        return clusters;
    }

    @Override
    public void setIndependentNodes(Set<Node> independentNodes) {
        this.independentNodes = independentNodes;
    }

    @Override
    public void addCluster(Cluster cluster) {
        //Add CLUSTER
        this.clusters.add(cluster);
        //Add CF to MQ
        this.modularizationQuality += cluster.getClusterFactor();
    }

    @Override
    public float getModularizationQuality() {
        return modularizationQuality;
    }

    @Override
    public void setModularizationQuality(float modularizationQuality) { this.modularizationQuality = modularizationQuality; }

    @Override
    public void addNodeToTargetCluster(Cluster cluster, Node node) {
        //Subtract old CF of target CLUSTER from MQ
        this.modularizationQuality -= cluster.getClusterFactor();
        //Add node to target CLUSTER
        cluster.addNode(node);
        //Ad new CF of target CLUSTER in MQ
        this.modularizationQuality += cluster.getClusterFactor();
    }

    @Override
    public void removeNodeFromTargetCluster(Cluster cluster, Node node) {
        //Subtract old CF of target CLUSTER from MQ
        this.modularizationQuality -= cluster.getClusterFactor();
        //Remove node from target CLUSTER
        cluster.removeNode(node);
        //Add new CF of target CLUSTER in MQ
        this.modularizationQuality += cluster.getClusterFactor();
    }

    @Override
    public void deleteEmptyClusters() {
        Iterator<Cluster> iterator = this.clusters.iterator();
        while (iterator.hasNext()) {
            Cluster cluster = iterator.next();
            if (cluster.getNodes().isEmpty()) {
                iterator.remove();
            }
        }
    }

    @Override
    public void setClusterIds(){
        final int[] i = {0};
        this.getClusters().forEach(cluster -> {
            cluster.setId(String.valueOf(i[0]));
            i[0]++;
        });
    }

    @Override
    public Cluster getClusterOfTargetNode(Node node){
        for(Cluster cluster : this.getClusters()){
            if(cluster.getNodes().contains(node)){ return cluster; }
        }
        return new ClusterImpl();
    }

    @Override
    public MDG clone() {
        MDG mdg = new MDGImpl();
        mdg.setIndependentNodes(getIndependentNodes());
        this.getClusters().forEach(cluster -> mdg.addCluster(cluster.clone()));
        mdg.setModularizationQuality(this.modularizationQuality);
        return mdg;
    }

}
