package model.api;

import java.util.Set;

/**
 * @author Georgia Grigoriadou
 */

public interface MDG {

    Set<Node> getIndependentNodes();

    Set<Cluster> getClusters();

    void setIndependentNodes(Set<Node> independentNodes);

    void addCluster(Cluster cluster);

    float getModularizationQuality();

    void setModularizationQuality(float modularizationQuality);

    void addNodeToTargetCluster(Cluster cluster, Node node);

    void removeNodeFromTargetCluster(Cluster cluster, Node node);

    void deleteEmptyClusters();

    void setClusterIds();

    Cluster getClusterOfTargetNode(Node node);

    MDG clone();

}
