package model;

import java.util.function.BiFunction;

/**
 * @author Georgia Grigoriadou
 */

@FunctionalInterface
public interface BiUnaryOperator<T> extends BiFunction<T, T, T> {

}
