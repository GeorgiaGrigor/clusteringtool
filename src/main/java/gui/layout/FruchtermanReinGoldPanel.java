package gui.layout;

import controller.visualization.Layout;
import gui.components.CollapsibleTable;
import gui.components.CustomTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class FruchtermanReinGoldPanel extends LayoutPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/FruchtermanReingold");

    private JTable table;

    FruchtermanReinGoldPanel(){
        super();

        initTable();

        startButton.addActionListener(
                e -> {
                    try {
                        if (table.isEditing()) table.getCellEditor().stopCellEditing();

                        float area     = Float.parseFloat((String) table.getModel().getValueAt(0, 1));
                        double gravity = Double.parseDouble((String) table.getModel().getValueAt(1, 1));
                        double speed   = Double.parseDouble((String) table.getModel().getValueAt(2, 1));

                        startLayout(Layout.setFruchtermanReinGoldLayout(area, gravity, speed));
                    } catch (NumberFormatException ex) {
                        showErrorMessage(ex);
                    }
                }
        );
        infoLabel.setToolTipText(bundle.getString("description"));
    }

    private void initTable(){
        table = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                String tip   = null;
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                if (colIndex == 1) {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } else {
                    switch (rowIndex) {
                        case 0:
                            tip = bundle.getString("fruchtermanReingold.area.desc");
                            break;
                        case 1:
                            tip = bundle.getString("fruchtermanReingold.gravity.desc");
                            break;
                        case 2:
                            tip = bundle.getString("fruchtermanReingold.speed.desc");
                            break;
                    }
                }
                return tip;
            }
        };
        DefaultTableModel model = (DefaultTableModel) table.getModel();

        Object[] row  = new Object[2];
        row[0]        = bundle.getString("fruchtermanReingold.area.name");
        row[1]        = "10000.0";
        Object[] row1 = new Object[2];
        row1[0]       = bundle.getString("fruchtermanReingold.gravity.name");
        row1[1]       = "10.0";
        Object[] row2 = new Object[2];
        row2[0]       = bundle.getString("fruchtermanReingold.speed.name");
        row2[1]       = "1.0";

        model.addRow(row);
        model.addRow(row1);
        model.addRow(row2);

        CollapsibleTable collapsibleTable = new CollapsibleTable(bundle.getString("name"), table);
        tablePanel.add(collapsibleTable);
    }

}
