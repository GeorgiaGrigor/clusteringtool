package gui.layout;

import gui.components.CollapsibleTable;
import gui.components.CustomTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class ScalePanel extends  LayoutPanel{
    ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Scale");

    JTable   table;
    Object[] row;

    ScalePanel(String scale) {
        super();
        initTable(scale);
    }

    private void initTable(String scale) {
        table = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                return colIndex == 1 ? getValueAt(rowIndex, colIndex).toString(): bundle.getString("ScaleLayout.scaleFactor.desc");
            }
        };

        row    = new Object[2];
        row[0] = bundle.getString("ScaleLayout.scaleFactor.name");
        row[1] = scale;
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.addRow(row);
        CollapsibleTable collapsibleTable = new CollapsibleTable(bundle.getString("ScaleLayout.properties"), table);
        tablePanel.add(collapsibleTable);
    }

}
