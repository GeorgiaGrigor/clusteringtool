package gui.layout;

import controller.visualization.Layout;
import gui.components.CollapsibleTable;
import gui.components.CustomTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.Random;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class OpenOrdPanel extends LayoutPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/OpenOrd");

    private JTable stagedTable;
    private JTable openOrdTable;

    OpenOrdPanel() {
        super();

        initTable();

        startButton.addActionListener(
                e -> {
                    try {
                        if (stagedTable.isEditing())  stagedTable.getCellEditor().stopCellEditing();
                        if (openOrdTable.isEditing()) openOrdTable.getCellEditor().stopCellEditing();

                        int[] intValues     = new int[7];
                        float[] floatValues = new float[2];
                        long randomSeed;

                        intValues[0]   = Integer.parseInt((String) stagedTable.getModel().getValueAt(0, 1));
                        intValues[1]   = Integer.parseInt((String) stagedTable.getModel().getValueAt(1, 1));
                        intValues[2]   = Integer.parseInt((String) stagedTable.getModel().getValueAt(2, 1));
                        intValues[3]   = Integer.parseInt((String) stagedTable.getModel().getValueAt(3, 1));
                        intValues[4]   = Integer.parseInt((String) stagedTable.getModel().getValueAt(4, 1));
                        intValues[5]   = Integer.parseInt((String) openOrdTable.getModel().getValueAt(1, 1));
                        intValues[6]   = Integer.parseInt((String) openOrdTable.getModel().getValueAt(2, 1));
                        floatValues[0] = Float.parseFloat((String) openOrdTable.getModel().getValueAt(0, 1));
                        floatValues[1] = Float.parseFloat((String) openOrdTable.getModel().getValueAt(3, 1));
                        randomSeed     = Long.parseLong((String) openOrdTable.getModel().getValueAt(4, 1));

                        startLayout(Layout.getOpenOrdLayout(intValues, floatValues, randomSeed));
                    } catch (NumberFormatException ex) {
                        showErrorMessage(ex);
                    }
                }
        );
        infoLabel.setToolTipText(bundle.getString("OpenOrd.description"));
    }

    private void initTable() {
        stagedTable = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                String tip   = null;
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                if (colIndex == 1) {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } else {
                    switch (rowIndex) {
                        case 0:
                            tip = bundle.getString("OpenOrd.properties.stage.liquid.description");
                            break;
                        case 1:
                            tip = bundle.getString("OpenOrd.properties.stage.expansion.description");
                            break;
                        case 2:
                            tip = bundle.getString("OpenOrd.properties.stage.cooldown.description");
                            break;
                        case 3:
                            tip = bundle.getString("OpenOrd.properties.stage.crunch.description");
                            break;
                        case 4:
                            tip = bundle.getString("OpenOrd.properties.stage.simmer.description");
                            break;
                    }
                }
                return tip;
            }
        };
        openOrdTable = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                String tip   = null;
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                if (colIndex == 1) {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } else {
                    switch (rowIndex) {
                        case 0:
                            tip = bundle.getString("OpenOrd.properties.edgecut.description");
                            break;
                        case 1:
                            tip = bundle.getString("OpenOrd.properties.numthreads.description");
                            break;
                        case 2:
                            tip = bundle.getString("OpenOrd.properties.numiterations.description");
                            break;
                        case 3:
                            tip = bundle.getString("OpenOrd.properties.realtime.description");
                            break;
                        case 4:
                            tip = bundle.getString("OpenOrd.properties.seed.description");
                            break;
                    }
                }
                return tip;
            }
        };

        DefaultTableModel stagesModel  = (DefaultTableModel) stagedTable.getModel();
        DefaultTableModel openOrdModel = (DefaultTableModel) openOrdTable.getModel();

        Random r         = new Random();
        Long randomSeed  = r.nextLong();

        Object[] row  = new Object[2];
        row[0]        = bundle.getString("OpenOrd.properties.stage.liquid.name");
        row[1]        = "25";
        Object[] row1 = new Object[2];
        row1[0]       = bundle.getString("OpenOrd.properties.stage.expansion.name");
        row1[1]       = "25";
        Object[] row2 = new Object[2];
        row2[0]       = bundle.getString("OpenOrd.properties.stage.cooldown.name");
        row2[1]       = "25";
        Object[] row3 = new Object[2];
        row3[0]       = bundle.getString("OpenOrd.properties.stage.crunch.name");
        row3[1]       = "10";
        Object[] row4 = new Object[2];
        row4[0]       = bundle.getString("OpenOrd.properties.stage.simmer.name");
        row4[1]       = "15";
        Object[] row5 = new Object[2];
        row5[0]       = bundle.getString("OpenOrd.properties.edgecut.name");
        row5[1]       = "0.8";
        Object[] row6 = new Object[2];
        row6[0]       = bundle.getString("OpenOrd.properties.numthreads.name");
        row6[1]       = "7";
        Object[] row7 = new Object[2];
        row7[0]       = bundle.getString("OpenOrd.properties.numiterations.name");
        row7[1]       = "750";
        Object[] row8 = new Object[2];
        row8[0]       = bundle.getString("OpenOrd.properties.realtime.name");
        row8[1]       = "0.2";
        Object[] row9 = new Object[2];
        row9[0]       = bundle.getString("OpenOrd.properties.seed.name");
        row9[1]       = String.valueOf(randomSeed);

        stagesModel.addRow(row);
        stagesModel.addRow(row1);
        stagesModel.addRow(row2);
        stagesModel.addRow(row3);
        stagesModel.addRow(row4);
        openOrdModel.addRow(row5);
        openOrdModel.addRow(row6);
        openOrdModel.addRow(row7);
        openOrdModel.addRow(row8);
        openOrdModel.addRow(row9);

        CollapsibleTable stagesCollapsibleTable  = new CollapsibleTable(bundle.getString("OpenOrd.stages"), stagedTable);
        CollapsibleTable openOrdCollapsibleTable = new CollapsibleTable(bundle.getString("OpenOrd.name"), openOrdTable);

        tablePanel.add(stagesCollapsibleTable);
        tablePanel.add(openOrdCollapsibleTable);
    }

}
