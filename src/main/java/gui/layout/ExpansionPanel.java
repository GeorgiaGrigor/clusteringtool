package gui.layout;

import controller.visualization.Layout;

/**
 * @author Gewrgia
 */

class ExpansionPanel extends ScalePanel {

    ExpansionPanel() {
        super("1.2");

        startButton.addActionListener(
                e -> {
                    try {
                        if (table.isEditing()) table.getCellEditor().stopCellEditing();
                        double scaleFactor = Double.parseDouble((String) table.getModel().getValueAt(0, 1));
                        startLayout(Layout.getExpansionLayout(scaleFactor));
                    } catch (NumberFormatException ex) {
                        showErrorMessage(ex);
                    }
                }
        );
        infoLabel.setToolTipText(bundle.getString("expand.description"));
    }

}
