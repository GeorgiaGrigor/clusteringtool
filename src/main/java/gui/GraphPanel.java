package gui;

import controller.visualization.Appearance;
import controller.visualization.Group;
import controller.visualization.Preview;
import gui.components.CustomColorPicker;
import gui.components.DropDownButton;
import gui.components.JFontChooser;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.preview.types.DependantOriginalColor;
import org.gephi.preview.types.EdgeColor;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Consumer;

/**
 * @author Georgia
 */

 class GraphPanel extends JPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Graph");

    private JPanel         toolBarPanel1;
    private JPanel         toolBarPanel2;
    private JPanel         toolBarPanel3;
    private JPanel         previewSketch;
    private JToolBar       westToolBar;
    private JToolBar       southToolBar;
    private JSlider        edgeThicknessSlider;
    private JSlider        fontSizeSlider;
    private JToggleButton  showLabelsButton;
    private JToggleButton  showEdgesButton;
    private JToggleButton  curvedEdgesButton;
    private JButton        backgroundButton;
    private JButton        fontButton;
    private JButton        labelColorButton;
    private JButton        resetZoomButton;
    private JButton        groupAllButton;
    private JButton        expandAllButton;
    private DropDownButton edgesModeButton;
    private DropDownButton labelSizeModeButton;
    private DropDownButton labelColorModeButton;


  GraphPanel() {
      initComponents();
  }

    void previewGraph() {
        previewSketch = Preview.getPreviewPanel();
        add(previewSketch, BorderLayout.CENTER);
        //Set Default values
        edgeThicknessSlider.setValue((int) Preview.edgeThickness * 10);
        edgeThicknessSlider.setMaximum((int) Preview.edgeThickness * 20);

        fontSizeSlider.setValue((int) Preview.labelSize * 10);
        fontSizeSlider.setMaximum((int) Preview.labelSize * 20);

        fontButton.setText(Preview.labelFont.getFontName() + ", " + Preview.labelFont.getSize());
        labelColorButton.setBackground(Preview.labelColor.getCustomColor());
        labelColorButton.setUI(new BasicButtonUI());

        showEdgesButton.setSelected(Preview.showEdges);
        curvedEdgesButton.setSelected(Preview.curveEdges);
        showLabelsButton.setSelected(Preview.showLabels);

        previewSketch.setDoubleBuffered(false);
    }

    private void initComponents() {
        toolBarPanel1         = new JPanel();
        toolBarPanel2         = new JPanel();
        toolBarPanel3         = new JPanel();
        westToolBar           = new JToolBar();
        southToolBar          = new JToolBar();
        edgeThicknessSlider   = new JSlider(JSlider.HORIZONTAL, 1, 20, 10);
        fontSizeSlider        = new JSlider(JSlider.HORIZONTAL, 1, 20, 10);
        showEdgesButton       = new JToggleButton();
        curvedEdgesButton     = new JToggleButton();
        showLabelsButton      = new JToggleButton();
        backgroundButton      = new JButton();
        fontButton            = new JButton();
        labelColorButton      = new JButton();
        resetZoomButton       = new JButton();
        groupAllButton        = new JButton();
        expandAllButton       = new JButton();
        edgesModeButton       = new DropDownButton();
        labelSizeModeButton   = new DropDownButton();
        labelColorModeButton  = new DropDownButton();

        fontButton.setText(bundle.getString("Graph.defaultFont"));

        Map<String, Consumer> labelSizeModeOptions = new LinkedHashMap<>();
        labelSizeModeOptions.put(bundle.getString("Graph.options.fixed"),   e -> Preview.setBooleanProperty(PreviewProperty.NODE_LABEL_PROPORTIONAL_SIZE, Boolean.FALSE));
        labelSizeModeOptions.put(bundle.getString("Graph.options.nodeSize"),e-> Preview.setBooleanProperty(PreviewProperty.NODE_LABEL_PROPORTIONAL_SIZE, Boolean.TRUE));
        labelSizeModeButton.setDropDownOptions(labelSizeModeOptions);

        Map<String, Consumer> labelColorModeOptions = new LinkedHashMap<>();
        labelColorModeOptions.put(bundle.getString("Graph.options.unique"), e -> Preview.setLabelColor(new DependantOriginalColor(DependantOriginalColor.Mode.ORIGINAL)));
        labelColorModeOptions.put(bundle.getString("Graph.options.object"), e -> Preview.setLabelColor(new DependantOriginalColor(DependantOriginalColor.Mode.PARENT)));
        labelColorModeOptions.put(bundle.getString("Graph.options.text"),   e -> Preview.setLabelColor(Preview.labelColor));
        labelColorModeButton.setDropDownOptions(labelColorModeOptions);

        Map<String, Consumer> edgeModeOptions = new LinkedHashMap<>();
        edgeModeOptions.put(bundle.getString("Graph.options.unique"), e -> Preview.setEdgeColor(new EdgeColor(EdgeColor.Mode.ORIGINAL)));
        edgeModeOptions.put(bundle.getString("Graph.options.source"), e -> Preview.setEdgeColor(new EdgeColor(EdgeColor.Mode.SOURCE)));
        edgeModeOptions.put(bundle.getString("Graph.options.target"), e -> Preview.setEdgeColor(new EdgeColor(EdgeColor.Mode.TARGET)));
        edgeModeOptions.put(bundle.getString("Graph.options.mixed"),  e -> Preview.setEdgeColor(new EdgeColor(EdgeColor.Mode.MIXED)));
        edgesModeButton.setDropDownOptions(edgeModeOptions);

        showEdgesButton.setSelected(true);
        showLabelsButton.setSelected(true);

        addToolTips();
        addIcons();
        addComponents();
        addListeners();
        setSize(southToolBar);
        setSize(westToolBar);
    }

    private void addListeners() {
        backgroundButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    new CustomColorPicker(true, false,
                            Preview::setBackgroundColor);
                }
                if (SwingUtilities.isLeftMouseButton(e)) {
                    Preview.setBackgroundColor();
                }
            }
        });
        showLabelsButton.addActionListener(e -> Preview.setBooleanProperty(PreviewProperty.SHOW_NODE_LABELS, showLabelsButton.isSelected()));
        showEdgesButton.addActionListener(e -> Preview.setBooleanProperty(PreviewProperty.SHOW_EDGES, showEdgesButton.isSelected()));
        edgeThicknessSlider.addChangeListener(e -> Preview.setEdgeThickness((float) edgeThicknessSlider.getValue() / 10));
        fontSizeSlider.addChangeListener(e -> Appearance.setUniqueLabelSize((double) fontSizeSlider.getValue() / 10));
        curvedEdgesButton.addActionListener(e -> Preview.setBooleanProperty(PreviewProperty.EDGE_CURVED, curvedEdgesButton.isSelected()));
        labelColorButton.addActionListener(e -> new CustomColorPicker(true, false,
                color -> {
                    Preview.setLabelColor(new DependantOriginalColor(color));
                    labelColorButton.setBackground(Preview.labelColor.getCustomColor());
                }
        ));
        fontButton.addActionListener(e -> {
            JFontChooser.setDefaultSelectedFont(Preview.labelFont);
            JFontChooser chooser = new JFontChooser();
            int option = chooser.showDialog(null);
            if (option == 0) {
                Font font = chooser.getSelectedFont();
                Preview.setLabelFont(font);

                fontButton.setText(font.getFontName() + ", " + font.getSize());
            }
        });
        resetZoomButton.addActionListener(e -> Preview.resetZoom());
        groupAllButton.addActionListener(e -> Group.preserveAppearance(Group::groupAll));
        expandAllButton.addActionListener(e -> Group.preserveAppearance(Group::expandAll));

    }

    private void addToolTips() {
        backgroundButton.createToolTip();
        backgroundButton.createToolTip();
        showEdgesButton.createToolTip();
        edgesModeButton.createToolTip();
        curvedEdgesButton.createToolTip();
        edgeThicknessSlider.createToolTip();
        showLabelsButton.createToolTip();
        labelColorButton.createToolTip();
        labelSizeModeButton.createToolTip();
        labelColorModeButton.createToolTip();
        fontButton.createToolTip();
        fontSizeSlider.createToolTip();
        backgroundButton.setToolTipText(bundle.getString("Graph.backgroundButtonTooltip"));
        showEdgesButton.setToolTipText(bundle.getString("Graph.showEdgesTooltip"));
        edgesModeButton.setToolTipText(bundle.getString("Graph.colorModeTooltip"));
        curvedEdgesButton.setToolTipText(bundle.getString("Graph.curveEdgesTooltip"));
        edgeThicknessSlider.setToolTipText(bundle.getString("Graph.edgeWeightTooltip"));
        showLabelsButton.setToolTipText(bundle.getString("Graph.showLabelTooltip"));
        labelColorButton.setToolTipText(bundle.getString("Graph.colorTooltip"));
        labelSizeModeButton.setToolTipText(bundle.getString("Graph.sizeModeTooltip"));
        labelColorModeButton.setToolTipText(bundle.getString("Graph.colorModeTooltip"));
        fontButton.setToolTipText(bundle.getString("Graph.fontTooltip"));
        fontSizeSlider.setToolTipText(bundle.getString("Graph.fontScaleTooltip"));
        resetZoomButton.setToolTipText(bundle.getString("Graph.resetZoomTooltip"));
        groupAllButton.setToolTipText(bundle.getString("Graph.groupAllTooltip"));
        expandAllButton.setToolTipText(bundle.getString("Graph.expandAllTooltip"));
    }

    private void addIcons() {
        backgroundButton.setIcon(new ImageIcon(bundle.getString("Graph.ideaIconPath")));
        showEdgesButton.setIcon(new ImageIcon(bundle.getString("Graph.lineIconPath")));
        edgesModeButton.setIcon(new ImageIcon(bundle.getString("Graph.edgeModeIconPath")));
        curvedEdgesButton.setIcon(new ImageIcon(bundle.getString("Graph.curveIconPath")));
        showLabelsButton.setIcon(new ImageIcon(bundle.getString("Graph.textIconPath")));
        labelSizeModeButton.setIcon(new ImageIcon(bundle.getString("Graph.genericTextIconPath")));
        labelColorModeButton.setIcon(new ImageIcon(bundle.getString("Graph.textColorIconPath")));
        labelColorButton.setIcon(new ImageIcon(bundle.getString("Graph.transParentIconPath")));
        resetZoomButton.setIcon(new ImageIcon(bundle.getString("Graph.resetZoomIconPath")));
        groupAllButton.setIcon(new ImageIcon(bundle.getString("Graph.groupIconPath")));
        expandAllButton.setIcon(new ImageIcon(bundle.getString("Graph.expandIconPath")));
    }

    private void addComponents() {
        setLayout(new BorderLayout());
        southToolBar.setFloatable(false);
        southToolBar.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        westToolBar.setFloatable(false);
        westToolBar.setOrientation(SwingConstants.VERTICAL);

        toolBarPanel1.setLayout(new FlowLayout(FlowLayout.LEFT,0 ,0));
        toolBarPanel2.setLayout(new FlowLayout(FlowLayout.LEFT,0 ,0));
        toolBarPanel3.setLayout(new FlowLayout(FlowLayout.LEFT,0 ,0));

        southToolBar.add(backgroundButton);
        southToolBar.addSeparator();
        southToolBar.add(showEdgesButton);
        southToolBar.add(edgesModeButton);
        southToolBar.add(curvedEdgesButton);
        southToolBar.add(edgeThicknessSlider);
        southToolBar.addSeparator();
        southToolBar.add(showLabelsButton);
        southToolBar.add(labelSizeModeButton);
        southToolBar.add(labelColorModeButton);
        southToolBar.add(fontButton);
        southToolBar.add(fontSizeSlider);
        southToolBar.add(labelColorButton);

        westToolBar.add(resetZoomButton);
        westToolBar.addSeparator();
        westToolBar.add(groupAllButton);
        westToolBar.add(expandAllButton);

        add(southToolBar, BorderLayout.SOUTH);
        add(westToolBar, BorderLayout.WEST);

    }

    void reset() {
        previewSketch.setVisible(false);
    }

    private void setSize(Container container) {
        for (Component component : container.getComponents()) {
            if (component instanceof Container) {
                setSize((Container) component);
            }
            if (component.getClass().equals(JButton.class)) {
                if (component != fontButton) {
                    component.setPreferredSize(new Dimension(20, 20));
                    component.setMinimumSize(new Dimension(20, 20));
                    component.setMaximumSize(new Dimension(20, 20));
                } else {
                    component.setPreferredSize(new Dimension(60, 20));
                    component.setMinimumSize(new Dimension(60, 20));
                    component.setMaximumSize(new Dimension(60, 20));
                }
            }
            if (component.getClass().equals(JToggleButton.class)) {
                component.setPreferredSize(new Dimension(20, 20));
                component.setMinimumSize(new Dimension(20, 20));
                component.setMaximumSize(new Dimension(20, 20));
            }
            if (component.getClass().equals(JSlider.class)) {
                component.setMaximumSize(new Dimension(120, 16));
                component.setMinimumSize(new Dimension(120, 16));
                component.setPreferredSize(new Dimension(120, 16));
            }
        }
    }

}