package gui.components.dialogs;

import controller.visualization.Appearance;
import controller.visualization.Group;
import controller.visualization.Preview;
import org.gephi.graph.api.Node;
import org.jfree.ui.tabbedui.VerticalLayout;
import controller.Project;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;

import static controller.visualization.GephiInitializer.CLUSTER;

/**
 * @author Gewrgia
 */

public class RenameDialog extends JDialog {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Components");

    private JTextField textField;
    private JLabel     alreadyExistsError;
    private JLabel     emptyError;

    private Node node;

    public RenameDialog(Node node){
        this.node = node;

        initComponents();

        setTitle(bundle.getString("Components.renameTitle") + node.getLabel());
        setSize(250, 130);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
        setModal(true);
        setVisible(true);
    }

    private void initComponents() {
        JPanel panel         = new JPanel();
        JPanel inputPanel    = new JPanel();
        JPanel errorPanel    = new JPanel();
        JPanel buttonPanel   = new JPanel();
        JLabel label         = new JLabel();
        JButton okButton     = new JButton();
        JButton cancelButton = new JButton();
        textField            = new JTextField();
        alreadyExistsError   = new JLabel();
        emptyError           = new JLabel();

        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        errorPanel.setPreferredSize(new Dimension(this.getWidth(), 20));

        label.setText(bundle.getString("Components.renameLabel"));
        alreadyExistsError.setText(bundle.getString("Components.alreadyExistsErrorMessage"));
        emptyError.setText(bundle.getString("Components.emptyErrorMessage"));
        textField.setText(node.getLabel());
        okButton.setText(bundle.getString("Components.ok"));
        cancelButton.setText(bundle.getString("Components.cancel"));

        textField.setPreferredSize(new Dimension(120, 20));
        textField.setInputVerifier(new ClusterIdVerifier());

        alreadyExistsError.setForeground(Color.RED);
        alreadyExistsError.setVisible(false);
        emptyError.setForeground(Color.RED);
        emptyError.setVisible(false);

        okButton.addActionListener(e ->{
            if (textField.getInputVerifier().verify(textField)) {
                String newID = textField.getText();
                Project.getMainPanel().rename(node.getLabel(), newID);

                Group.getPartitionMap().remove(node.getLabel());
                Group.getPartitionMap().put(newID, true);
                Project.getMdg().getClusters()
                        .stream()
                        .filter(cluster -> cluster.getId().equals(node.getLabel()))
                        .forEach(cluster -> cluster.setId(newID));

                node.setLabel(newID);
                node.getTextProperties().setText(newID);
                node.setAttribute(CLUSTER, newID);
                Preview.refreshPreview();
                dispose();
            }
        });
        cancelButton.addActionListener(e -> dispose());

        panel.setLayout(new VerticalLayout());
        inputPanel.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
        errorPanel.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));

        inputPanel.add(label);
        inputPanel.add(textField);

        errorPanel.add(alreadyExistsError);
        errorPanel.add(emptyError);

        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        panel.add(inputPanel);
        panel.add(errorPanel);
        panel.add(buttonPanel);

        setContentPane(panel);
    }

    private class ClusterIdVerifier extends InputVerifier {

        @Override
        public boolean verify(JComponent input) {
            return !Appearance.partition.getSortedValues().contains(((JTextField) input).getText()) && ((JTextField) input).getText().trim().length() > 0;
        }

        @Override
        public boolean shouldYieldFocus(JComponent input) {
            alreadyExistsError.setVisible(Appearance.partition.getSortedValues().contains(((JTextField) input).getText()));
            emptyError.setVisible(((JTextField) input).getText().trim().length() <= 0);
            return verify(input);
        }
    }

}
