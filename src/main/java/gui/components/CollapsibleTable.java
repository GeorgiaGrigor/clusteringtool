package gui.components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

/**
 * @author Georgia Grigoriadou
 */

public class CollapsibleTable  extends JPanel{
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Components");

    private JPanel titlePanel;
    private JLabel titleLabel;
    private JTable table;

    public CollapsibleTable(String title, JTable jtable){
        initComponents(title, jtable);
    }

    private void initComponents(String title, JTable jtable){
        titlePanel = new JPanel();
        titleLabel = new JLabel(title);
        table      = jtable;

        titlePanel.setBackground(new Color(210, 210, 210));
        titleLabel.setIcon(new ImageIcon(bundle.getString("Components.minusIconPath")));

        titlePanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                table.setVisible(!table.isVisible());
                if(table.isVisible()){
                    titleLabel.setIcon(new ImageIcon(bundle.getString("Components.minusIconPath")));
                }
                else {
                    titleLabel.setIcon(new ImageIcon(bundle.getString("Components.plusIconPath")));
                }
            }
        });

        titlePanel.setLayout(new BorderLayout());
        titlePanel.add(titleLabel, BorderLayout.WEST);
        setLayout(new BorderLayout());
        add(titlePanel, BorderLayout.NORTH);
        add(table, BorderLayout.CENTER);
    }

}
