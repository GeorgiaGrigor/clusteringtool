package gui.appearance;

import controller.visualization.Appearance;
import gui.components.CustomColorPicker;
import org.jfree.ui.tabbedui.VerticalLayout;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import java.util.ResourceBundle;

/**
 * @author Georgia Grigoriadou
 */

class EdgeColorPanel extends AppearancePanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Appearance");

    private JPanel       uniqueOptionPanel;
    private JPanel       intraInterOptionPanel;
    private JPanel       rankingOptionPanel;
    private JPanel       rankingColorPanel;
    private JRadioButton uniqueRadioButton;
    private JRadioButton intraInterRadioButton;
    private JRadioButton rankingRadioButton;
    private JLabel       intraLabel;
    private JLabel       interLabel;
    private JLabel       colorLabel;
    private JButton      colorButton;
    private JButton      intraButton;
    private JButton      interButton;
    private JButton      rankingColorButton1;
    private JButton      rankingColorButton2;

    private Color uniqueEdgeColor    = Color.BLACK;
    private Color intraEdgeColor     = Color.RED;
    private Color interEdgeColor     = Color.BLUE;
    private Color rankingEdgeColor1  = Color.WHITE;
    private Color rankingEdgeColor2  = Color.BLACK;

    EdgeColorPanel() {
        super();
        initComponents();
    }

    private void initComponents() {
        uniqueOptionPanel     = new JPanel();
        intraInterOptionPanel = new JPanel();
        rankingOptionPanel    = new JPanel();
        rankingColorPanel     = new JPanel();
        uniqueRadioButton     = new JRadioButton();
        intraInterRadioButton = new JRadioButton();
        rankingRadioButton    = new JRadioButton();
        colorLabel            = new JLabel();
        intraLabel            = new JLabel();
        interLabel            = new JLabel();
        colorButton           = new JButton();
        intraButton           = new JButton();
        interButton           = new JButton();
        rankingColorButton1   = new JButton();
        rankingColorButton2   = new JButton();

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(uniqueRadioButton);
        buttonGroup.add(intraInterRadioButton);
        buttonGroup.add(rankingRadioButton);

        uniqueRadioButton.setText(bundle.getString("Appearance.unique"));
        intraInterRadioButton.setText(bundle.getString("Appearance.intraInterEdges"));
        rankingRadioButton.setText(bundle.getString("Appearance.ranking"));

        colorLabel.setText("#ffffff");
        intraLabel.setText(bundle.getString("Appearance.intraEdges"));
        interLabel.setText(bundle.getString("Appearance.interEdges"));

        colorButton.setBackground(Color.BLACK);
        intraButton.setBackground(Color.RED);
        interButton.setBackground(Color.BLUE);
        rankingColorButton1.setBackground(Color.WHITE);
        rankingColorButton2.setBackground(Color.BLACK);

        colorButton.setUI(new BasicButtonUI());
        intraButton.setUI(new BasicButtonUI());
        interButton.setUI(new BasicButtonUI());
        rankingColorButton1.setUI(new BasicButtonUI());
        rankingColorButton2.setUI(new BasicButtonUI());

        colorButton.setPreferredSize(new Dimension(16, 16));
        intraButton.setPreferredSize(new Dimension(16, 16));
        interButton.setPreferredSize(new Dimension(16, 16));
        rankingColorButton1.setPreferredSize(new Dimension(16, 16));
        rankingColorButton2.setPreferredSize(new Dimension(16, 16));

        addComponents();
        addListeners();

        uniqueOptionPanel.setVisible(false);
        intraInterOptionPanel.setVisible(false);
        rankingOptionPanel.setVisible(false);
    }

    private void addComponents() {
        uniqueOptionPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        intraInterOptionPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        rankingOptionPanel.setLayout(new VerticalLayout());
        rankingColorPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        panel.add(uniqueRadioButton);
        uniqueOptionPanel.add(colorButton);
        uniqueOptionPanel.add(colorLabel);
        panel.add(uniqueOptionPanel);
        panel.add(intraInterRadioButton);
        intraInterOptionPanel.add(intraLabel);
        intraInterOptionPanel.add(intraButton);
        intraInterOptionPanel.add(interLabel);
        intraInterOptionPanel.add(interButton);
        panel.add(intraInterOptionPanel);
        panel.add(rankingRadioButton);
        rankingColorPanel.add(rankingColorButton1);
        rankingColorPanel.add(rankingColorButton2);
        rankingOptionPanel.add(rankingColorPanel);
        panel.add(rankingOptionPanel);
    }

    @Override
    void apply() {
        if (uniqueRadioButton.isSelected()) {
            Appearance.setUniqueEdgeColor(uniqueEdgeColor);
        }
        if (intraInterRadioButton.isSelected()) {
            Appearance.setInterIntraEdgeColor(intraEdgeColor, interEdgeColor);
        }
        if (rankingRadioButton.isSelected()) {
            Appearance.setRankingEdgeColor(rankingEdgeColor1, rankingEdgeColor2);
        }
    }

    private void addListeners() {
        uniqueRadioButton.addActionListener(e -> {
            if (enableAutoTransformation) {
                Appearance.setUniqueEdgeColor(uniqueEdgeColor);
            }
            uniqueOptionPanel.setVisible(true);
            intraInterOptionPanel.setVisible(false);
            rankingOptionPanel.setVisible(false);
        });
        intraInterRadioButton.addActionListener(e -> {
            if (enableAutoTransformation) {
                Appearance.setInterIntraEdgeColor(intraEdgeColor, interEdgeColor);
            }
            uniqueOptionPanel.setVisible(false);
            intraInterOptionPanel.setVisible(true);
            rankingOptionPanel.setVisible(false);
        });
        rankingRadioButton.addActionListener(e -> {
            if (enableAutoTransformation) {
                Appearance.setRankingEdgeColor(rankingEdgeColor1, rankingEdgeColor2);
            }
            uniqueOptionPanel.setVisible(false);
            intraInterOptionPanel.setVisible(false);
            rankingOptionPanel.setVisible(true);
        });
        colorButton.addActionListener(e -> new CustomColorPicker(true, false,
                color -> {
                    if (enableAutoTransformation) {
                        Appearance.setUniqueEdgeColor(color);
                    }
                    colorButton.setBackground(color);
                    colorLabel.setText("#" + Integer.toHexString(color.getRGB()).substring(2));
                    uniqueEdgeColor = color;
                }));
        intraButton.addActionListener(e -> new CustomColorPicker(true, false,
                color -> {
                    if (enableAutoTransformation) {
                        Appearance.setInterIntraEdgeColor(color, interEdgeColor);
                    }
                    intraButton.setBackground(color);
                    intraEdgeColor = color;
                }));
        interButton.addActionListener(e -> new CustomColorPicker(true, false,
                color -> {
                    if (enableAutoTransformation) {
                        Appearance.setInterIntraEdgeColor(intraEdgeColor, color);
                    }
                    interButton.setBackground(color);
                    interEdgeColor = color;
                }));
        rankingColorButton1.addActionListener(e -> new CustomColorPicker(true, false,
                color -> {
                    if (enableAutoTransformation) {
                        Appearance.setRankingEdgeColor(color, rankingEdgeColor2);
                    }
                    rankingColorButton1.setBackground(color);
                    rankingEdgeColor1 = color;
                }));
        rankingColorButton2.addActionListener(e -> new CustomColorPicker(true, false,
                color -> {
                    if (enableAutoTransformation) {
                        Appearance.setRankingEdgeColor(rankingEdgeColor1, color);
                    }
                    rankingColorButton2.setBackground(color);
                    rankingEdgeColor2 = color;
                }));
    }

}
