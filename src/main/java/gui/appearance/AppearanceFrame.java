package gui.appearance;

import com.sun.java.swing.plaf.windows.WindowsTabbedPaneUI;
import controller.visualization.Appearance;

import javax.swing.*;
import javax.swing.plaf.basic.BasicBorders;
import javax.swing.plaf.synth.SynthInternalFrameUI;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.util.ResourceBundle;

/**
 * @author Georgia Grigoriadou
 */

public class AppearanceFrame extends JInternalFrame {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Appearance");

    private JPanel          panel;
    private JPanel          elementPanel;
    private JPanel          propertyPanel;
    private JPanel          footerPanel;
    private JPanel          edgeColorPanel;
    private JPanel          nodeColorPanel;
    private JPanel          nodeSizePanel;
    private JPanel          labelSizePanel;
    private JPanel          labelColorPanel;
    private JTabbedPane     tabbedPane;
    private JToolBar        toolBar;
    private JToggleButton   nodeButton;
    private JToggleButton   edgeButton;
    private JToggleButton   labelButton;
    private JToggleButton   colorButton;
    private JToggleButton   sizeButton;
    private JToggleButton   autoTransformButton;
    private JButton         applyButton;


    public AppearanceFrame() {
        setTitle(bundle.getString("Appearance.name"));
        setFrameIcon(new ImageIcon(bundle.getString("Appearance.appearanceIconPath")));

        initComponents();

        setContentPane(panel);
        setBorder(new BasicBorders.MarginBorder());
        setClosable(true);
        setVisible(true);

        SynthInternalFrameUI ui = (SynthInternalFrameUI) getUI();
        ui.propertyChange(new PropertyChangeEvent(this, JInternalFrame.IS_SELECTED_PROPERTY, 0, 0));
    }

    private void initComponents() {
        UIManager.put("TabbedPane.contentBorderInsets", new Insets(0, 0, 0, 0));

        panel               = new JPanel();
        elementPanel        = new JPanel();
        propertyPanel       = new JPanel();
        footerPanel         = new JPanel();
        edgeColorPanel      = new JPanel();
        nodeColorPanel      = new JPanel();
        nodeSizePanel       = new JPanel();
        labelSizePanel      = new JPanel();
        labelColorPanel     = new JPanel();
        tabbedPane          = new JTabbedPane();
        toolBar             = new JToolBar();
        nodeButton          = new JToggleButton();
        edgeButton          = new JToggleButton();
        labelButton         = new JToggleButton();
        colorButton         = new JToggleButton();
        sizeButton          = new JToggleButton();
        autoTransformButton = new JToggleButton();
        applyButton         = new JButton();

        ButtonGroup elementButtonGroup = new ButtonGroup();
        elementButtonGroup.add(nodeButton);
        elementButtonGroup.add(edgeButton);
        elementButtonGroup.add(labelButton);

        ButtonGroup propertyButtonGroup = new ButtonGroup();
        propertyButtonGroup.add(colorButton);
        propertyButtonGroup.add(sizeButton);

        nodeButton.setText(bundle.getString("Appearance.node"));
        edgeButton.setText(bundle.getString("Appearance.edge"));
        labelButton.setText(bundle.getString("Appearance.label"));

        Insets insets = new Insets(0, 0, 0, 0);
        autoTransformButton.setIcon(new ImageIcon(bundle.getString("Appearance.autoIconPath")));
        autoTransformButton.setToolTipText(bundle.getString("Appearance.autoTransformation"));
        autoTransformButton.setFocusPainted(false);
        autoTransformButton.setMargin(insets);
        autoTransformButton.setContentAreaFilled(false);
        autoTransformButton.setBorderPainted(false);
        autoTransformButton.setOpaque(false);

        applyButton.setIcon(new ImageIcon(bundle.getString("Appearance.playIconPath")));
        applyButton.setText(bundle.getString("Appearance.apply"));

        nodeButton.setMargin(insets);
        edgeButton.setMargin(insets);
        labelButton.setMargin(insets);
        colorButton.setMargin(insets);
        sizeButton.setMargin(insets);

        colorButton.setBackground(null);
        sizeButton.setBackground(null);

        Dimension d = new Dimension(40, 22);
        nodeButton.setMinimumSize(d);
        edgeButton.setMinimumSize(d);
        labelButton.setMinimumSize(d);

        nodeButton.setMaximumSize(d);
        edgeButton.setMaximumSize(d);
        labelButton.setMaximumSize(d);

        nodeButton.setPreferredSize(d);
        edgeButton.setPreferredSize(d);
        labelButton.setPreferredSize(d);

        colorButton.setIcon(new ImageIcon(bundle.getString("Appearance.paletteIconPath")));
        sizeButton.setIcon(new ImageIcon(bundle.getString("Appearance.lowercaseIconPath")));

        nodeButton.setSelected(true);
        colorButton.setSelected(true);

        toolBar.setBorder(null);

        addComponents();
        addListeners();


        tabbedPane.setUI(new WindowsTabbedPaneUI());
        UIManager.put("TabbedPane.tabInsets", new Insets(-10, -10, -10, -10));

    }

    private void addComponents() {
        setLayout(new BorderLayout());
        panel.setLayout(new BorderLayout());
        elementPanel.setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
        propertyPanel.setLayout(new FlowLayout(FlowLayout.RIGHT,0,0));
        footerPanel.setLayout(new FlowLayout(FlowLayout.RIGHT,0,0));

        elementPanel.add(nodeButton);
        elementPanel.add(edgeButton);
        elementPanel.add(labelButton);

        propertyPanel.add(colorButton);
        propertyPanel.add(sizeButton);

        toolBar.add(elementPanel);
        toolBar.add(propertyPanel);

        toolBar.setFloatable(false);
        footerPanel.add(autoTransformButton);
        footerPanel.add(applyButton);

        tabbedPane.add(nodeColorPanel, 0);
        tabbedPane.add(nodeSizePanel, 1);
        tabbedPane.add(edgeColorPanel, 2);
        tabbedPane.add(labelColorPanel, 3);
        tabbedPane.add(labelSizePanel, 4);

        panel.add(toolBar, BorderLayout.NORTH);
        panel.add(tabbedPane, BorderLayout.CENTER);
        panel.add(footerPanel, BorderLayout.SOUTH);
    }

    private void addListeners(){
        nodeButton.addActionListener(e -> {
            int index = colorButton.isSelected()? 0 : 1;
            tabbedPane.setSelectedIndex(index);
            sizeButton.setVisible(true);
        });
        edgeButton.addActionListener(e ->{
            tabbedPane.setSelectedIndex(2);
            sizeButton.setVisible(false);
        });
        labelButton.addActionListener(e ->{
            int index = colorButton.isSelected()? 3 : 4;
            tabbedPane.setSelectedIndex(index);
            sizeButton.setVisible(true);
        });
        colorButton.addActionListener(e->{
            int index = nodeButton.isSelected()? 0 : labelButton.isSelected()? 3 : 2;
            tabbedPane.setSelectedIndex(index);
        });
        sizeButton.addActionListener(e->{
            int index = nodeButton.isSelected()? 1 : 4;
            tabbedPane.setSelectedIndex(index);
        });

        applyButton.addActionListener(e->{
            for (Component c: tabbedPane.getComponents()){
                if(AppearancePanel.class.isAssignableFrom(c.getClass())) {
                    ((AppearancePanel) c).apply();
                }
            }
        });

        autoTransformButton.addActionListener(e ->{
            for (Component c: tabbedPane.getComponents()){
                if(AppearancePanel.class.isAssignableFrom(c.getClass())) {
                    ((AppearancePanel) c).enableAutoTransformation(autoTransformButton.isSelected());
                }
            }
            applyButton.setEnabled(!autoTransformButton.isSelected());
        });
    }

    public void initAppearance(){
        Appearance.init();
        tabbedPane.removeAll();
        edgeColorPanel      = new EdgeColorPanel();
        nodeColorPanel      = new NodeColorPanel();
        nodeSizePanel       = new NodeSizePanel();
        labelSizePanel      = new LabelSizePanel();
        labelColorPanel     = new LabelColorPanel();

        tabbedPane.add(nodeColorPanel, 0);
        tabbedPane.add(nodeSizePanel, 1);
        tabbedPane.add(edgeColorPanel, 2);
        tabbedPane.add(labelColorPanel, 3);
        tabbedPane.add(labelSizePanel, 4);
    }

    public void reset(){
        tabbedPane.removeAll();
        edgeColorPanel      = new JPanel();
        nodeColorPanel      = new JPanel();
        nodeSizePanel       = new JPanel();
        labelSizePanel      = new JPanel();
        labelColorPanel     = new JPanel();

        tabbedPane.add(nodeColorPanel, 0);
        tabbedPane.add(nodeSizePanel, 1);
        tabbedPane.add(edgeColorPanel, 2);
        tabbedPane.add(labelColorPanel, 3);
        tabbedPane.add(labelSizePanel, 4);

        nodeButton.setSelected(true);
        colorButton.setSelected(true);
    }

    public void setVisible(boolean b) {
        boolean visible = isVisible();
        super.setVisible(b);
        firePropertyChange("visible", visible, b);
    }

}
