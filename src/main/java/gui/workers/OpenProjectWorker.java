package gui.workers;

import gui.resources.FileProperties;
import controller.Project;

import javax.swing.*;

/**
 * @author Gewrgia
 */

public class OpenProjectWorker  extends SwingWorker<Void, Integer> {

    private JProgressBar   progressBar;
    private FileProperties file;
    private Runnable       runnable;

    public OpenProjectWorker(JProgressBar jpb, FileProperties file, Runnable runnable) {
        this.progressBar = jpb;
        this.file        = file;
        this.runnable    = runnable;
    }

    @Override
    protected Void doInBackground() throws Exception {
        if (file != null) {
            progressBar.setIndeterminate(true);
            progressBar.setVisible(true);
            Project.open(file);
        }
        return null;
    }

    @Override
    protected void done() {
        runnable.run();
        progressBar.setVisible(false);
        progressBar.setIndeterminate(false);
    }

}
