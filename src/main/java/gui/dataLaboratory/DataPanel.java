package gui.dataLaboratory;

import com.sun.java.swing.plaf.windows.WindowsTabbedPaneUI;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

public class DataPanel extends JPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Table");

    private TablePanel  nodesPanel;
    private TablePanel  edgesPanel;
    private TablePanel  clusterPanel;
    private JTabbedPane tabbedPane;

    public DataPanel(){
        initComponents();
    }

    private void initComponents() {
        JPanel buttonPanel           = new JPanel();
        JToggleButton nodesButton    = new JToggleButton();
        JToggleButton edgesButton    = new JToggleButton();
        JToggleButton clustersButton = new JToggleButton();

        nodesPanel   = new NodesTablePanel();
        edgesPanel   = new EdgesTablePanel();
        clusterPanel = new ClusterTablePanel();
        tabbedPane   = new JTabbedPane();

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(nodesButton);
        buttonGroup.add(edgesButton);
        buttonGroup.add(clustersButton);

        nodesButton.setText(bundle.getString("Table.nodesButton"));
        edgesButton.setText(bundle.getString("Table.edgesButton"));
        clustersButton.setText(bundle.getString("Table.clustersButton"));

        tabbedPane.add(nodesPanel, 0);
        tabbedPane.add(edgesPanel, 1);
        tabbedPane.add(clusterPanel, 2);

        UIManager.put("TabbedPane.tabInsets", new Insets(-10, -10, -10, -10));
        tabbedPane.setUI(new WindowsTabbedPaneUI());

        nodesButton.setSelected(true);

        setLayout(new BorderLayout());
        buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        buttonPanel.add(nodesButton);
        buttonPanel.add(edgesButton);
        buttonPanel.add(clustersButton);

        add(buttonPanel, BorderLayout.NORTH);
        add(tabbedPane, BorderLayout.CENTER);

        nodesButton.addActionListener(e -> tabbedPane.setSelectedIndex(0));
        edgesButton.addActionListener(e -> tabbedPane.setSelectedIndex(1));
        clustersButton.addActionListener(e -> tabbedPane.setSelectedIndex(2));
    }

    public void init(){
        nodesPanel.init();
        edgesPanel.init();
        clusterPanel.init();
    }

    public void reset(){
        nodesPanel.reset();
        edgesPanel.reset();
        clusterPanel.init();
    }

    public void rename(String oldID, String newID){
        nodesPanel.rename(oldID, newID);
        clusterPanel.rename(oldID, newID);
    }

}
