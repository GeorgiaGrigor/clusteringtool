package gui.filter;

import controller.visualization.Filter;

import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class FilterEdgesPanel extends FilterPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Filter");

    private ClusterPanel intraPanel;
    private ClusterPanel interPanel;


    FilterEdgesPanel() {}

    @SuppressWarnings("unchecked")
	@Override
    void init() {
        intraPanel = new ClusterPanel(bundle.getString("Filter.intraEdges"));
        interPanel = new ClusterPanel(bundle.getString("Filter.interEdges"));

        panel.add(intraPanel);
        panel.add(interPanel);
    }

    @Override
    boolean createFilters() {
        Filter.createIntraEdgeQuery(intraPanel.getSelectedClusters());
        Filter.createInterEdgeQuery(interPanel.getSelectedClusters());
        return true;
    }

    @Override
    void rename(String oldID, String newID) {
        intraPanel.rename(oldID, newID);
        interPanel.rename(oldID, newID);
    }

    @Override
    void reset() {
        panel.removeAll();
        init();
        scrollPane.setViewportView(panel);
    }

}
