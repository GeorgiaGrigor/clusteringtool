package gui.filter;

import controller.visualization.Filter;
import controller.visualization.GephiInitializer;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import javax.swing.plaf.basic.BasicBorders;
import java.awt.*;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class FilterNodesPanel extends FilterPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Filter");

    private ClusterPanel       clusterPanel;
    private JCheckBox          independentCheckBox;
    private JCheckBox          hideNodesCheckBox;
    private JCheckBox          neighboursCheckBox;
    private JCheckBox          clusterSizeCheckBox;
    private JCheckBox          clusterFactorCheckBox;
    private JCheckBox          degreeCheckBox;
    private JCheckBox          inDegreeCheckBox;
    private JCheckBox          outDegreeCheckBox;
    private RangeOptionsPanel  clusterSizeOptions;
    private RangeOptionsPanel  clusterFactorOptions;
    private RangeOptionsPanel  degreeOptions;
    private RangeOptionsPanel  inDegreeOptions;
    private RangeOptionsPanel  outDegreeOptions;

    FilterNodesPanel() {}

    @SuppressWarnings("unchecked")
	@Override
    void init() {
        clusterPanel          = new ClusterPanel(bundle.getString("Filter.clusters"));
        independentCheckBox   = new JCheckBox();
        hideNodesCheckBox     = new JCheckBox();
        neighboursCheckBox    = new JCheckBox();
        clusterSizeCheckBox   = new JCheckBox();
        clusterFactorCheckBox = new JCheckBox();
        degreeCheckBox        = new JCheckBox();
        inDegreeCheckBox      = new JCheckBox();
        outDegreeCheckBox     = new JCheckBox();
        clusterSizeOptions    = new RangeOptionsPanel(new IntegerVerifier());
        clusterFactorOptions  = new RangeOptionsPanel(new FloatVerifier());
        degreeOptions         = new RangeOptionsPanel(new IntegerVerifier());
        inDegreeOptions       = new RangeOptionsPanel(new IntegerVerifier());
        outDegreeOptions      = new RangeOptionsPanel(new IntegerVerifier());

        independentCheckBox.setSelected(true);
        independentCheckBox.setText(bundle.getString("Filter.showIndependent"));
        independentCheckBox.setBackground(Color.WHITE);

        hideNodesCheckBox.setSelected(false);
        hideNodesCheckBox.setText(bundle.getString("Filter.hideNodes"));
        hideNodesCheckBox.setBackground(Color.WHITE);

        neighboursCheckBox.setSelected(false);
        neighboursCheckBox.setText(bundle.getString("Filter.showNeighbours"));
        neighboursCheckBox.setBackground(Color.WHITE);

        clusterSizeCheckBox.setSelected(false);
        clusterSizeCheckBox.setText(bundle.getString("Filter.clusterSize"));
        clusterSizeCheckBox.setBackground(Color.WHITE);
        clusterSizeCheckBox.addActionListener(e -> clusterSizeOptions.setVisible(clusterSizeCheckBox.isSelected()));

        clusterFactorCheckBox.setSelected(false);
        clusterFactorCheckBox.setText(bundle.getString("Filter.clusterFactor"));
        clusterFactorCheckBox.setBackground(Color.WHITE);
        clusterFactorCheckBox.addActionListener(e -> clusterFactorOptions.setVisible(clusterFactorCheckBox.isSelected()));

        degreeCheckBox.setSelected(false);
        degreeCheckBox.setText(bundle.getString("Filter.degree"));
        degreeCheckBox.setBackground(Color.WHITE);
        degreeCheckBox.addActionListener(e -> degreeOptions.setVisible(degreeCheckBox.isSelected()));

        inDegreeCheckBox.setSelected(false);
        inDegreeCheckBox.setText(bundle.getString("Filter.inDegree"));
        inDegreeCheckBox.setBackground(Color.WHITE);
        inDegreeCheckBox.addActionListener(e -> inDegreeOptions.setVisible(inDegreeCheckBox.isSelected()));

        outDegreeCheckBox.setSelected(false);
        outDegreeCheckBox.setText(bundle.getString("Filter.outDegree"));
        outDegreeCheckBox.setBackground(Color.WHITE);
        outDegreeCheckBox.addActionListener(e -> outDegreeOptions.setVisible(outDegreeCheckBox.isSelected()));

        clusterSizeOptions.setVisible(false);
        clusterFactorOptions.setVisible(false);
        degreeOptions.setVisible(false);
        inDegreeOptions.setVisible(false);
        outDegreeOptions.setVisible(false);

        panel.add(independentCheckBox);
        panel.add(hideNodesCheckBox);
        panel.add(clusterPanel);
        panel.add(clusterSizeCheckBox);
        panel.add(clusterSizeOptions);
        panel.add(clusterFactorCheckBox);
        panel.add(clusterFactorOptions);
        panel.add(degreeCheckBox);
        panel.add(degreeOptions);
        panel.add(inDegreeCheckBox);
        panel.add(inDegreeOptions);
        panel.add(outDegreeCheckBox);
        panel.add(outDegreeOptions);
        panel.add(neighboursCheckBox);
    }

    @Override
    void rename(String oldID, String newID) {
        clusterPanel.rename(oldID, newID);
    }

    @Override
    boolean createFilters() {
        ArrayList<String> clusters = clusterPanel.getSelectedClusters();
        if (independentCheckBox.isSelected()) clusters.add(GephiInitializer.INDEPENDENT);
        Filter.setHideNodes(hideNodesCheckBox.isSelected());
        Filter.setShowNeighbours(neighboursCheckBox.isSelected());
        if (clusterSizeOptions.hasValidValues()) {
            Filter.setClusterSizeFilter(clusterSizeCheckBox.isSelected(),(int) clusterSizeOptions.getMinValue(),(int) clusterSizeOptions.getMaxValue());
        } else {
            return false;
        }
        if (clusterFactorOptions.hasValidValues()) {
            Filter.setClusterFactorFilter(clusterFactorCheckBox.isSelected(), clusterFactorOptions.getMinValue(), clusterFactorOptions.getMaxValue());
        } else {
            return false;
        }
        Filter.createNodeQuery(clusters);
        if (degreeCheckBox.isSelected()) {
            if (degreeOptions.hasValidValues()) {
                Filter.createDegreeQuery((int) degreeOptions.getMinValue(),(int) degreeOptions.getMaxValue());
            } else {
                return false;
            }
        }
        if (inDegreeCheckBox.isSelected()) {
            if (inDegreeOptions.hasValidValues()) {
                Filter.createInDegreeQuery((int) inDegreeOptions.getMinValue(),(int) inDegreeOptions.getMaxValue());
            } else {
                return false;
            }
        }
        if (outDegreeCheckBox.isSelected()) {
            if (outDegreeOptions.hasValidValues()) {
                Filter.createOutDegreeQuery((int) outDegreeOptions.getMinValue(),(int) outDegreeOptions.getMaxValue());
            } else {
                return false;
            }
        }
        return true;
    }

    @Override
    void reset() {
        panel.removeAll();
        init();
        scrollPane.setViewportView(panel);
    }

    private class RangeOptionsPanel extends JPanel {
        private JTextField    minValue;
        private JTextField    maxValue;
        private InputVerifier verifier;

        RangeOptionsPanel(InputVerifier inputVerifier) {
            super();
            verifier = inputVerifier;
            initComponents();
        }

        private void initComponents() {
            JLabel minValueLabel = new JLabel();
            JLabel maxValueLabel = new JLabel();

            minValue = new JTextField();
            maxValue = new JTextField();

            Dimension dimension = new Dimension(45, 20);
            minValue.setPreferredSize(dimension);
            minValue.setMinimumSize(dimension);
            minValue.setMaximumSize(dimension);
            maxValue.setPreferredSize(dimension);

            minValueLabel.setText("min: ");
            maxValueLabel.setText("max: ");
            minValue.setInputVerifier(verifier);
            maxValue.setInputVerifier(verifier);
            minValue.setText(bundle.getString("Filter.auto"));
            maxValue.setText(bundle.getString("Filter.auto"));

            add(minValueLabel);
            add(minValue);
            add(maxValueLabel);
            add(maxValue);
        }

        float getMinValue() {
            try {
                return Float.parseFloat(minValue.getText());
            } catch (NumberFormatException e) {
                return Float.MIN_VALUE;
            }
        }

        float getMaxValue() {
            try {
                return Float.parseFloat(maxValue.getText());
            } catch (NumberFormatException e) {
                return Float.MAX_VALUE;
            }
        }

        boolean hasValidValues(){
            return minValue.getInputVerifier().verify(minValue) && maxValue.getInputVerifier().verify(maxValue);
        }
    }

    private class IntegerVerifier extends InputVerifier {
        JTextField textField = new JTextField();

        @Override
        public boolean verify(JComponent input) {
            String text = ((JTextField) input).getText();
            return StringUtils.isNumeric(text) || text.equals(bundle.getString("Filter.auto"));
        }

        @Override
        public boolean shouldYieldFocus(JComponent input) {
            if (!verify(input)) {
                input.setBorder(new BasicBorders.FieldBorder(Color.RED, Color.RED, Color.RED, Color.RED));
                JOptionPane.showMessageDialog(null, bundle.getString("Filter.nonLegalValue") + ((JTextField) input).getText());
            } else {
                input.setBorder(textField.getBorder());
            }
            return verify(input);
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private class FloatVerifier extends InputVerifier {
        JTextField textField = new JTextField();

        @Override
        public boolean verify(JComponent input) {
            String text = ((JTextField) input).getText();
            if(!text.equals(bundle.getString("Filter.auto"))){
                try{
                    Float.parseFloat(text);
                } catch (NumberFormatException e){
                    return false;
                }
            }
            return true;
        }

        @Override
        public boolean shouldYieldFocus(JComponent input) {
            if (!verify(input)) {
                input.setBorder(new BasicBorders.FieldBorder(Color.RED, Color.RED, Color.RED, Color.RED));
                JOptionPane.showMessageDialog(null, bundle.getString("Filter.nonLegalValue") + ((JTextField) input).getText());
            } else {
                input.setBorder(textField.getBorder());
            }
            return verify(input);
        }
    }

}
