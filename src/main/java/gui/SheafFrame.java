package gui;

import de.javasoft.plaf.synthetica.SyntheticaSimple2DLookAndFeel;
import controller.visualization.Appearance;
import controller.visualization.Filter;
import controller.visualization.GephiInitializer;
import controller.visualization.Preview;
import gui.appearance.AppearanceFrame;
import gui.components.CustomFileChooser;
import gui.components.dialogs.AboutDialog;
import gui.components.dialogs.NewProjectDialog;
import gui.dataLaboratory.DataPanel;
import gui.filter.FilterFrame;
import gui.layout.LayoutFrame;
import gui.resources.FileProperties;
import gui.resources.History;
import gui.workers.NewProjectWorker;
import gui.workers.OpenProjectWorker;
import controller.Project;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.util.ResourceBundle;

import static java.awt.Event.SHIFT_MASK;

/**
 * @author Georgia Grigoriadou
 */

public class SheafFrame extends JFrame {
    private ResourceBundle mainBundle = ResourceBundle.getBundle("Bundle/Main");

    public SheafFrame() {

        ImageIcon img   = new ImageIcon(mainBundle.getString("Main.sheafIcon"));
        MainPanel panel = new MainPanel();
        Project.setMainPanel(panel);

        setTitle("Sheaf");
        setIconImage(img.getImage());
        setContentPane(panel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                History.readHistoryFile();
            }
        });
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                History.writeHistoryFile();
            }
        });
        pack();
        setVisible(true);
    }

    public class MainPanel extends JPanel {
        private ResourceBundle menuBundle = ResourceBundle.getBundle("Bundle/Menu");

        private JSplitPane horizontalSplitPanel;
        private JSplitPane horizontalSplitPanel2;
        private JSplitPane verticalSplitPanel;
        private JPanel mainPanel;
        private JPanel menuPanel;
        private JPanel footerPanel;
        private JPanel centerPanel;
        private JPanel previewPanel;
        private DataPanel dataPanel;
        private AppearanceFrame appearanceFrame;
        private LayoutFrame layoutFrame;
        private FilterFrame filterFrame;
        private GraphPanel graphPanel;
        private JProgressBar progressBar;
        private JMenuBar menuBar;
        private JMenu fileMenu;
        private JMenu openRecentMenu;
        private JMenu exportMenu;
        private JMenu windowMenu;
        private JMenu helpMenu;
        private JMenuItem newProjectMenuItem;
        private JMenuItem openMenuItem;
        private JMenuItem closeProjectMenuItem;
        private JMenuItem saveMenuItem;
        private JMenuItem saveAsMenuItem;
        private JMenuItem exportAsGraphMenuItem;
        private JMenuItem exportAsImageMenuItem;
        private JMenuItem exitMenuItem;
        private JMenuItem aboutMenuItem;
        private JCheckBoxMenuItem appearanceMenuItem;
        private JCheckBoxMenuItem layoutMenuItem;
        private JCheckBoxMenuItem filterMenuItem;

        @SuppressWarnings("WeakerAccess")
        public MainPanel() {
            try {
                UIManager.setLookAndFeel(new SyntheticaSimple2DLookAndFeel());
            } catch (ParseException | UnsupportedLookAndFeelException e) {
                e.printStackTrace();
            }
            initComponents();
        }

        private void initComponents() {

            JTabbedPane tabbedPane = new JTabbedPane();
            horizontalSplitPanel = new JSplitPane();
            horizontalSplitPanel2 = new JSplitPane();
            verticalSplitPanel = new JSplitPane();
            mainPanel = new JPanel();
            menuPanel = new JPanel();
            footerPanel = new JPanel();
            centerPanel = new JPanel();
            previewPanel = new JPanel();
            dataPanel = new DataPanel();
            graphPanel = new GraphPanel();
            appearanceFrame = new AppearanceFrame();
            layoutFrame = new LayoutFrame();
            filterFrame = new FilterFrame();
            progressBar = new JProgressBar();

            initMenu();
            setLayouts();
            setIcons();
            addMenu();
            addListeners();

            appearanceFrame.addPropertyChangeListener("visible", e -> {
                appearanceMenuItem.setSelected((boolean) e.getNewValue());
                setVerticalSplitPanels();
            });
            layoutFrame.addPropertyChangeListener("visible", e -> {
                layoutMenuItem.setSelected((boolean) e.getNewValue());
                setVerticalSplitPanels();
            });
            filterFrame.addPropertyChangeListener("visible", e -> {
                filterMenuItem.setSelected((boolean) e.getNewValue());
                if ((boolean) e.getNewValue()) {
                    horizontalSplitPanel2.setDividerSize(5);
                    horizontalSplitPanel2.setDividerLocation(.85d);
                    horizontalSplitPanel2.setResizeWeight(.85d);
                } else {
                    horizontalSplitPanel2.setDividerSize(0);
                }
            });

            verticalSplitPanel.setOrientation(JSplitPane.VERTICAL_SPLIT);
            verticalSplitPanel.setTopComponent(appearanceFrame);
            verticalSplitPanel.setBottomComponent(layoutFrame);
            verticalSplitPanel.setDividerSize(5);
            verticalSplitPanel.setDividerLocation(.5d);
            verticalSplitPanel.setResizeWeight(.5d);

            horizontalSplitPanel.setLeftComponent(verticalSplitPanel);
            horizontalSplitPanel.setRightComponent(graphPanel);
            horizontalSplitPanel.setDividerSize(5);
            horizontalSplitPanel.setDividerLocation(.2d);
            horizontalSplitPanel.setResizeWeight(.2d);

            horizontalSplitPanel2.setLeftComponent(horizontalSplitPanel);
            horizontalSplitPanel2.setRightComponent(filterFrame);
            horizontalSplitPanel2.setDividerSize(5);
            horizontalSplitPanel2.setDividerLocation(.85d);
            horizontalSplitPanel2.setResizeWeight(.85d);
            previewPanel.add(horizontalSplitPanel2, BorderLayout.CENTER);

            footerPanel.setPreferredSize(new Dimension(mainPanel.getWidth(), 18));
            progressBar.setPreferredSize(new Dimension(200, 15));
            progressBar.setVisible(false);

            tabbedPane.addTab(mainBundle.getString("Main.preview"), previewPanel);
            tabbedPane.addTab(mainBundle.getString("Main.dataLaboratory"), dataPanel);
            centerPanel.add(tabbedPane, BorderLayout.CENTER);
            mainPanel.add(centerPanel, BorderLayout.CENTER);
            footerPanel.add(progressBar, BorderLayout.EAST);
            add(mainPanel, BorderLayout.CENTER);
            add(footerPanel, BorderLayout.SOUTH);
            enableComponents(previewPanel, false);
            enableComponents(dataPanel, false);

        }

        private void initMenu() {
            menuBar               = new JMenuBar();
            fileMenu              = new JMenu();
            openRecentMenu        = new JMenu();
            exportMenu            = new JMenu();
            windowMenu            = new JMenu();
            helpMenu              = new JMenu();
            newProjectMenuItem    = new JMenuItem();
            openMenuItem          = new JMenuItem();
            closeProjectMenuItem  = new JMenuItem();
            saveMenuItem          = new JMenuItem();
            saveAsMenuItem        = new JMenuItem();
            exportAsGraphMenuItem = new JMenuItem();
            exportAsImageMenuItem = new JMenuItem();
            exitMenuItem          = new JMenuItem();
            aboutMenuItem         = new JMenuItem();
            appearanceMenuItem    = new JCheckBoxMenuItem();
            layoutMenuItem        = new JCheckBoxMenuItem();
            filterMenuItem        = new JCheckBoxMenuItem();
            //Set text for Menus
            fileMenu.setText(menuBundle.getString("Menu.fileMenu"));
            openRecentMenu.setText(menuBundle.getString("Menu.openRecentMenu"));
            exportMenu.setText(menuBundle.getString("Menu.exportMenu"));
            windowMenu.setText(menuBundle.getString("Menu.windowMenu"));
            helpMenu.setText(menuBundle.getString("Menu.helpMenu"));

            //Set text for MenuItems
            newProjectMenuItem.setText(menuBundle.getString("Menu.newProjectMenuItem"));
            openMenuItem.setText(menuBundle.getString("Menu.openMenuItem"));
            closeProjectMenuItem.setText(menuBundle.getString("Menu.closeMenuItem"));
            saveMenuItem.setText(menuBundle.getString("Menu.saveMenuItem"));
            saveAsMenuItem.setText(menuBundle.getString("Menu.saveAsMenuItem"));
            exportAsGraphMenuItem.setText(menuBundle.getString("Menu.graphFileMenuItem"));
            exportAsImageMenuItem.setText(menuBundle.getString("Menu.imageFileMenuItem"));
            exitMenuItem.setText(menuBundle.getString("Menu.exitMenuItem"));
            aboutMenuItem.setText(menuBundle.getString("Menu.aboutMenuItem"));
            appearanceMenuItem.setText(menuBundle.getString("Menu.appearanceMenuItem"));
            layoutMenuItem.setText(menuBundle.getString("Menu.layoutMenuItem"));
            filterMenuItem.setText(menuBundle.getString("Menu.filterMenuItem"));

            //Disable buttons
            closeProjectMenuItem.setEnabled(false);
            saveMenuItem.setEnabled(false);
            saveAsMenuItem.setEnabled(false);
            exportMenu.setEnabled(false);

            //Select CheckBoxMenuItems
            appearanceMenuItem.setSelected(true);
            layoutMenuItem.setSelected(true);
            filterMenuItem.setSelected(true);

            //Add hotkeys
            newProjectMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK + SHIFT_MASK));
            openMenuItem.setAccelerator(KeyStroke.getKeyStroke('O', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
            saveMenuItem.setAccelerator(KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        }

        private void addMenu() {
            fileMenu.add(newProjectMenuItem);
            fileMenu.add(openMenuItem);
            fileMenu.add(openRecentMenu);
            fileMenu.add(closeProjectMenuItem);
            fileMenu.addSeparator();
            fileMenu.add(saveMenuItem);
            fileMenu.add(saveAsMenuItem);
            fileMenu.addSeparator();
            exportMenu.add(exportAsGraphMenuItem);
            exportMenu.add(exportAsImageMenuItem);
            fileMenu.add(exportMenu);
            fileMenu.addSeparator();
            fileMenu.add(exitMenuItem);

            windowMenu.add(appearanceMenuItem);
            windowMenu.add(layoutMenuItem);
            windowMenu.add(filterMenuItem);

            helpMenu.add(aboutMenuItem);

            menuBar.add(fileMenu);
            menuBar.add(windowMenu);
            menuBar.add(helpMenu);
            menuPanel.add(menuBar);
            mainPanel.add(menuPanel, BorderLayout.NORTH);
        }

        private void setLayouts() {
            setLayout(new BorderLayout());
            mainPanel.setLayout(new BorderLayout());
            menuPanel.setLayout(new BorderLayout());
            centerPanel.setLayout(new BorderLayout());
            previewPanel.setLayout(new BorderLayout());
            footerPanel.setLayout(new BorderLayout());
        }

        private void setIcons() {
            newProjectMenuItem.setIcon(new ImageIcon(menuBundle.getString("Menu.newFileIconPath")));
            openMenuItem.setIcon(new ImageIcon(menuBundle.getString("Menu.openFileIconPath")));
            saveMenuItem.setIcon(new ImageIcon(menuBundle.getString("Menu.saveIconPath")));
            saveAsMenuItem.setIcon(new ImageIcon(menuBundle.getString("Menu.saveIconPath")));
            exitMenuItem.setIcon(new ImageIcon(menuBundle.getString("Menu.exitIconPath")));
        }

        private void addListeners() {
            newProjectMenuItem.addActionListener(this::newProjectMenuItemActionPerformed);
            openMenuItem.addActionListener(this::openActionPerformed);
            openRecentMenu.addMenuListener(new MenuListener() {
                @Override
                public void menuCanceled(MenuEvent e) {
                }

                @Override
                public void menuDeselected(MenuEvent e) {
                    openRecentMenuMenuDeselected();
                }

                @Override
                public void menuSelected(MenuEvent e) {
                    openRecentMenuMenuSelected();
                }
            });
            closeProjectMenuItem.addActionListener(this::closeProjectMenuItemActionPerformed);
            saveMenuItem.addActionListener(e -> save());
            saveAsMenuItem.addActionListener(e -> saveAs());
            exportAsGraphMenuItem.addActionListener(this::exportAsGraphActionPerformed);
            exportAsImageMenuItem.addActionListener(this::exportAsImageActionPerformed);
            exitMenuItem.addActionListener(this::exitMenuItemActionPerformed);
            aboutMenuItem.addActionListener(e -> new AboutDialog());
            appearanceMenuItem.addActionListener(
                    e -> appearanceFrame.setVisible(appearanceMenuItem.isSelected())
            );
            layoutMenuItem.addActionListener(
                    e -> layoutFrame.setVisible(layoutMenuItem.isSelected())
            );
            filterMenuItem.addActionListener(
                    e -> filterFrame.setVisible(filterMenuItem.isSelected())
            );
        }

        private void newProjectMenuItemActionPerformed(ActionEvent e) {
            saveBeforeClose(() -> {
                NewProjectDialog dialog = new NewProjectDialog();
                String[] paths = dialog.getPaths();
                SwingWorker worker = new NewProjectWorker(progressBar, paths, dialog.getInitialPopulation(), dialog.weightEdges(), this::initView);
                worker.execute();
            });
        }

        private void openActionPerformed(ActionEvent e) {
            saveBeforeClose(() -> {
                CustomFileChooser fileChooser = new CustomFileChooser();
                FileProperties file = fileChooser.showOpenGraphDialog();
                SwingWorker worker = new OpenProjectWorker(progressBar, file, this::initView);
                worker.execute();
            });
        }

        private void openRecentMenuMenuSelected() {
            for (FileProperties file : History.getMostRecentFiles(10)) {
                JMenuItem menuItem = new JMenuItem(file.getName());
                menuItem.addActionListener((event) -> saveBeforeClose(() -> {
                    SwingWorker worker = new OpenProjectWorker(progressBar, file, this::initView);
                    worker.execute();
                }));
                openRecentMenu.add(menuItem);
            }
        }

        private void openRecentMenuMenuDeselected() {
            openRecentMenu.removeAll();
        }

        private void closeProjectMenuItemActionPerformed(ActionEvent e) {
            saveBeforeClose(() -> {
            });
        }

        private void exportAsGraphActionPerformed(ActionEvent e) {
            CustomFileChooser fileChooser = new CustomFileChooser();
            FileProperties file = fileChooser.showExportGraphDialog();
            if (file != null) {
                Project.exportAsGraph(file, fileChooser.isVisibleOnly());
            }
        }

        private void exportAsImageActionPerformed(ActionEvent e) {
            CustomFileChooser fileChooser = new CustomFileChooser();
            FileProperties file = fileChooser.showExportAsImageDialog();
            if (file != null) {
                Project.exportAsImage(file);
            }
        }

        private void exitMenuItemActionPerformed(ActionEvent e) {
            int save = showSaveDialog();
            switch (save) {
                case 0:
                    save();
                    System.exit(0);
                    break;
                case 1:
                    System.exit(0);
                    break;
            }
        }

        private void saveAs() {
            CustomFileChooser fileChooser = new CustomFileChooser();
            FileProperties file = fileChooser.showSaveAsDialog();
            if (file != null) {
                Project.saveAs(file);
            }
        }

        private void save() {
            if (Project.isSaved() || Project.getFileProperties().getLocation().endsWith(".controller.visualization")) {
                Project.saveAs(Project.getFileProperties());
            } else {
                saveAs();
            }
        }

        private int showSaveDialog() {
            return JOptionPane.showConfirmDialog(
                    null,
                    menuBundle.getString("Menu.saveBeforeCloseMessage"),
                    menuBundle.getString("Menu.saveBeforeCloseMessage"),
                    JOptionPane.YES_NO_CANCEL_OPTION
            );
        }

        private void saveBeforeClose(Runnable runnable) {
            if (Project.getGraphModel() != null) {
                int save = showSaveDialog();
                switch (save) {
                    case 0:
                        save();
                        resetView();
                        break;
                    case 1:
                        resetView();
                        break;
                    case 2:
                        return;
                }
            }
            runnable.run();
        }

        private void enableComponents(Container container, boolean enable) {
            Component[] components = container.getComponents();
            for (Component component : components) {
                component.setEnabled(enable);
                if (component instanceof Container) {
                    enableComponents((Container) component, enable);
                }
            }
        }

        private void setVerticalSplitPanels() {
            verticalSplitPanel.setVisible(layoutFrame.isVisible() || appearanceFrame.isVisible());
            if (layoutFrame.isVisible() && appearanceFrame.isVisible()) {
                verticalSplitPanel.setDividerSize(5);
                verticalSplitPanel.setDividerLocation(.4d);
                verticalSplitPanel.setResizeWeight(.4d);
                return;
            } else {
                verticalSplitPanel.setDividerSize(0);
            }
            if (layoutFrame.isVisible() || appearanceFrame.isVisible()) {
                horizontalSplitPanel.setDividerSize(5);
                horizontalSplitPanel.setDividerLocation(.25d);
                horizontalSplitPanel.setResizeWeight(.25d);
            } else {
                horizontalSplitPanel.setDividerSize(0);
            }
        }

        private void initView() {
            if (Project.getGraphModel() != null) {
                closeProjectMenuItem.setEnabled(true);
                saveMenuItem.setEnabled(true);
                saveAsMenuItem.setEnabled(true);
                exportMenu.setEnabled(true);
                appearanceFrame.setEnabled(true);

                appearanceFrame.initAppearance();
                filterFrame.initFilters();
                graphPanel.previewGraph();

                verticalSplitPanel.setDividerSize(5);
                verticalSplitPanel.setDividerLocation(.4d);
                verticalSplitPanel.setResizeWeight(.4d);
                horizontalSplitPanel.setDividerSize(5);
                horizontalSplitPanel.setDividerLocation(.25d);
                horizontalSplitPanel.setResizeWeight(.25d);
                horizontalSplitPanel2.setDividerSize(5);
                horizontalSplitPanel2.setDividerLocation(.85d);
                horizontalSplitPanel2.setResizeWeight(.85d);

                enableComponents(centerPanel, true);
                enableComponents(dataPanel, true);
                dataPanel.init();
            }
        }

        private void resetView() {
            closeProjectMenuItem.setEnabled(false);
            saveMenuItem.setEnabled(false);
            saveAsMenuItem.setEnabled(false);
            exportMenu.setEnabled(false);

            graphPanel.reset();
            appearanceFrame.reset();
            layoutFrame.reset();
            filterFrame.reset();
            dataPanel.reset();
            enableComponents(previewPanel, false);
            enableComponents(dataPanel, false);

            Project.reset();
            GephiInitializer.reset();
            Preview.reset();
            Appearance.reset();
            Filter.reset();
        }

        public void rename(String oldID, String newID) {
            dataPanel.rename(oldID, newID);
            filterFrame.rename(oldID, newID);
        }
    }
}


