package gui.resources;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Gewrgia
 */

@XmlRootElement(name = "recentFile")
public class FileProperties {
    private String name;
    private String location;
    private String type;

    @SuppressWarnings("unused")
    public FileProperties(){}

    public FileProperties(String name, String location, String type) {
        if (name.contains(".")) {
            this.name = name.substring(0, name.indexOf("."));
        } else {
            this.name = name;
        }
        this.location = location;
        this.type     = type;
    }

    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        if (name.contains(".")) {
            this.name = name.substring(0, name.indexOf("."));
        } else {
            this.name = name;
        }
    }

    public String getLocation() {
        return location;
    }

    @XmlElement
    @SuppressWarnings("unused")
    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    @XmlElement
    public void setType(String type) {
        this.type = type;
    }

}
