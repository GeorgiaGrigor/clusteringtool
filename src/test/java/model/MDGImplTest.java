package model;

import model.api.Cluster;
import model.api.MDG;
import model.api.Node;
import model.impl.ClusterImpl;
import model.impl.MDGImpl;
import model.impl.NodeImpl;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Georgia Grigoriadou
 */

@SuppressWarnings("all")
public class MDGImplTest {

    @Test
    public void testGetClusters()throws Exception{
        final MDGImpl mdg = new MDGImpl();
        final Field field = mdg.getClass().getDeclaredField("clusters");
        field.setAccessible(true);
        Set<Cluster> clusters  = new HashSet<>();
        Cluster cluster        = new ClusterImpl();
        clusters.add(cluster);
        field.set(mdg, clusters);

        Set<Cluster> result = mdg.getClusters();

        Assert.assertEquals(clusters, result);
    }

    @Test
    public void testGetModularizationQuality()throws Exception{
        final MDGImpl mdg = new MDGImpl();
        final Field field = mdg.getClass().getDeclaredField("modularizationQuality");
        field.setAccessible(true);
        float modularizationQuality = (float) 1.0;

        field.set(mdg, modularizationQuality);
        float result = mdg.getModularizationQuality();

        Assert.assertEquals(modularizationQuality, result, 0.0);
    }

    @Test
    public void testSetModularizationQuality()throws Exception{
        final MDGImpl mdg           = new MDGImpl();
        float modularizationQuality = (float) 1.0;

        mdg.setModularizationQuality(modularizationQuality);

        final Field field = mdg.getClass().getDeclaredField("modularizationQuality");
        field.setAccessible(true);

        Assert.assertEquals(field.get(mdg), modularizationQuality);
    }

    @Test
    public void testAddCluster()throws Exception{
        final MDGImpl mdg = new MDGImpl();
        Cluster cluster   = new ClusterImpl();
        Node node         = new NodeImpl(1, "testNode");
        cluster.addNode(node);
        mdg.addCluster(cluster);

        final Field field = mdg.getClass().getDeclaredField("clusters");
        field.setAccessible(true);

        Set<Cluster> clusters = (Set<Cluster>) field.get(mdg);

        Assert.assertTrue(clusters.contains(cluster));
    }

    @Test
    public void testDeleteEmptyClusters() throws Exception{
        final MDGImpl mdg      = new MDGImpl();
        Set<Cluster> clusters  = new HashSet<>();
        Cluster cluster        = new ClusterImpl();
        Cluster cluster1       = new ClusterImpl();
        Cluster cluster2       = new ClusterImpl();
        Node node              = new NodeImpl(0, "testNode");
        Node node1             = new NodeImpl(1, "testNode");
        Node node2             = new NodeImpl(2, "testNode");
        cluster.addNode(node);
        cluster.addNode(node1);
        cluster1.addNode(node2);
        clusters.add(cluster);
        clusters.add(cluster1);
        clusters.add(cluster2);

        final Field field = mdg.getClass().getDeclaredField("clusters");
        field.setAccessible(true);
        field.set(mdg, clusters);

        mdg.deleteEmptyClusters();
        Set<Cluster> result = (Set<Cluster>) field.get(mdg);

        Assert.assertEquals(result.size(), 2);
        Assert.assertFalse(result.contains(cluster2));
    }

    @Test
    public void testSetClusterIds() throws Exception{
        final MDGImpl mdg      = new MDGImpl();
        Set<Cluster> clusters  = new HashSet<>();
        Cluster cluster        = new ClusterImpl();
        Cluster cluster1       = new ClusterImpl();
        Cluster cluster2       = new ClusterImpl();
        clusters.add(cluster);
        clusters.add(cluster1);
        clusters.add(cluster2);

        final Field field = mdg.getClass().getDeclaredField("clusters");
        field.setAccessible(true);
        field.set(mdg, clusters);

        mdg.setClusterIds();
        Set<Cluster> result = (Set<Cluster>) field.get(mdg);

        for(int i = 0; i < 3; i++){
            boolean found = false;
            for(Cluster c : result){
                if(c.getId().equals(String.valueOf(i))){
                    found = true;
                }
            }
            Assert.assertTrue(found);
        }
    }

    @Test
    public void testClone() throws Exception{
        final MDGImpl mdg                        = new MDGImpl();
        float modularizationQuality              = 0;
        Set<Cluster> clusters                    = new HashSet<>();
        Cluster cluster                          = new ClusterImpl();
        Node node                                = new NodeImpl(0, "testNode");
        HashMap<Integer, Cluster> nodeClusterMap = new HashMap<>();

        clusters.add(cluster);
        cluster.addNode(node);
        nodeClusterMap.put(0, cluster);

        final Field clustersField = mdg.getClass().getDeclaredField("clusters");
        final Field mqField       = mdg.getClass().getDeclaredField("modularizationQuality");

        clustersField.setAccessible(true);
        mqField.setAccessible(true);

        clustersField.set(mdg, clusters);
        mqField.set(mdg, modularizationQuality);

        MDG newMDG = mdg.clone();

        Assert.assertNotEquals(newMDG.getClusters(), clusters);
        Assert.assertTrue(newMDG.getClusters().iterator().next().getNodes().contains(node));
        Assert.assertEquals(newMDG.getModularizationQuality(), modularizationQuality, 0.0);
    }

    @Test
    public void testAddNodeToTargetCluster() throws Exception{
        final MDGImpl mdg                        = new MDGImpl();
        Set<Cluster> clusters                    = new HashSet<>();
        float modularizationQuality              = 0;
        HashMap<Integer, Cluster> nodeClusterMap = new HashMap<>();
        Cluster cluster                          = new ClusterImpl();
        Cluster cluster1                         = new ClusterImpl();
        Node node                                = new NodeImpl(0, "testNode");
        Node node1                               = new NodeImpl(1, "testNode");
        Node node2                               = new NodeImpl(2, "testNode");
        node.addNeighbour(node1, (float) 1.0);
        node.addNeighbour(node2, (float) 1.0);
        node1.addNeighbour(node, (float) 1.0);
        node1.addNeighbour(node2, (float) 1.0);
        node2.addNeighbour(node, (float) 1.0);
        node2.addNeighbour(node1, (float) 1.0);
        cluster.addNode(node1);
        cluster1.addNode(node2);
        cluster.setClusterFactor((float) 0.0);
        cluster1.setClusterFactor((float) 0.0);
        clusters.add(cluster);
        clusters.add(cluster1);
        nodeClusterMap.put(1, cluster);
        nodeClusterMap.put(2, cluster1);

        final Field clustersField = mdg.getClass().getDeclaredField("clusters");
        final Field mqField = mdg.getClass().getDeclaredField("modularizationQuality");

        clustersField.setAccessible(true);
        mqField.setAccessible(true);

        clustersField.set(mdg, clusters);
        mqField.set(mdg, modularizationQuality);

        mdg.addNodeToTargetCluster(cluster, node);

        Assert.assertEquals(mqField.get(mdg), (float) 0.5);

    }

    @Test
    public void testRemoveNodeFromTargetCluster() throws Exception{
        final MDGImpl mdg                        = new MDGImpl();
        Set<Cluster> clusters                    = new HashSet<>();
        float modularizationQuality              = (float) 0.5;
        HashMap<Integer, Cluster> nodeClusterMap = new HashMap<>();
        Cluster cluster                          = new ClusterImpl();
        Cluster cluster1                         = new ClusterImpl();
        Node node                                = new NodeImpl(0, "testNode");
        Node node1                               = new NodeImpl(1, "testNode");
        Node node2                               = new NodeImpl(2, "testNode");
        node.addNeighbour(node1, (float) 1.0);
        node.addNeighbour(node2, (float) 1.0);
        node1.addNeighbour(node, (float) 1.0);
        node1.addNeighbour(node2, (float) 1.0);
        node2.addNeighbour(node, (float) 1.0);
        node2.addNeighbour(node1, (float) 1.0);
        cluster.addNode(node);
        cluster.addNode(node1);
        cluster1.addNode(node2);
        cluster.setClusterFactor((float) 0.5);
        cluster1.setClusterFactor((float) 0.0);
        clusters.add(cluster);
        clusters.add(cluster1);
        nodeClusterMap.put(0, cluster);
        nodeClusterMap.put(1, cluster);
        nodeClusterMap.put(2, cluster1);

        final Field clustersField = mdg.getClass().getDeclaredField("clusters");
        final Field mqField = mdg.getClass().getDeclaredField("modularizationQuality");

        clustersField.setAccessible(true);
        mqField.setAccessible(true);

        clustersField.set(mdg, clusters);
        mqField.set(mdg, modularizationQuality);

        mdg.removeNodeFromTargetCluster(cluster, node);
        Set<Cluster> resultClusters = (Set<Cluster>) clustersField.get(mdg);

        Assert.assertFalse(resultClusters.iterator().next().getNodes().contains(node));
        Assert.assertEquals(mqField.get(mdg), (float) 0.0);

    }
}
