package controller;

import controller.clustering.RandomClusterGenerator;
import model.TriFunction;
import model.api.*;
import model.impl.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.ToIntFunction;

/**
 * @author Georgia Grigoriadou
 */

@SuppressWarnings("unchecked")
public class RandomClusterGeneratorTest {
    private Graph graph  = new GraphImpl();

    @Before
    @SuppressWarnings("Duplicates")
    public void setup() {
        Node nodeA  = new NodeImpl(1, "A");
        Node nodeB  = new NodeImpl(2, "B");
        Node nodeC  = new NodeImpl(3, "C");
        Node nodeD  = new NodeImpl(4, "D");
        Node nodeE  = new NodeImpl(5, "E");
        Node nodeF  = new NodeImpl(6, "F");
        Edge edge1  = new EdgeImpl(1, 2, "Directed", 7);
        Edge edge2  = new EdgeImpl(1, 3, "Directed", 1);
        Edge edge3  = new EdgeImpl(2, 3, "Directed", 5);
        Edge edge4  = new EdgeImpl(5, 2, "Directed", 2);
        Edge edge5  = new EdgeImpl(4, 2, "Directed", 4);
        Edge edge6  = new EdgeImpl(2, 6, "Directed", 1);
        Edge edge7  = new EdgeImpl(3, 6, "Directed", 7);
        Edge edge8  = new EdgeImpl(3, 5, "Directed", 2);
        Edge edge9  = new EdgeImpl(4, 5, "Directed", 5);
        Edge edge10 = new EdgeImpl(5, 6, "Directed", 1);

        nodeA.addNeighbour(nodeB, (float) 7.0);
        nodeA.addNeighbour(nodeC, (float) 1.0);
        nodeB.addNeighbour(nodeA, (float) 7.0);
        nodeB.addNeighbour(nodeC, (float) 5.0);
        nodeB.addNeighbour(nodeD, (float) 4.0);
        nodeB.addNeighbour(nodeE, (float) 2.0);
        nodeB.addNeighbour(nodeF, (float) 1.0);
        nodeC.addNeighbour(nodeA, (float) 1.0);
        nodeC.addNeighbour(nodeB, (float) 5.0);
        nodeC.addNeighbour(nodeE, (float) 2.0);
        nodeC.addNeighbour(nodeF, (float) 7.0);
        nodeD.addNeighbour(nodeB, (float) 4.0);
        nodeD.addNeighbour(nodeE, (float) 5.0);
        nodeE.addNeighbour(nodeB, (float) 2.0);
        nodeE.addNeighbour(nodeC, (float) 2.0);
        nodeE.addNeighbour(nodeD, (float) 4.0);
        nodeE.addNeighbour(nodeF, (float) 1.0);
        nodeF.addNeighbour(nodeB, (float) 1.0);
        nodeF.addNeighbour(nodeC, (float) 7.0);
        nodeF.addNeighbour(nodeE, (float) 1.0);

        Set<Node> nodes = new HashSet<>();
        Set<Edge> edges = new HashSet<>();

        nodes.add(nodeA);
        nodes.add(nodeB);
        nodes.add(nodeC);
        nodes.add(nodeD);
        nodes.add(nodeE);
        nodes.add(nodeF);
        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);
        edges.add(edge6);
        edges.add(edge7);
        edges.add(edge8);
        edges.add(edge9);
        edges.add(edge10);

        graph.setNodes(nodes);
        graph.setEdges(edges);
    }

    @Test
    public void testGenerate() throws Exception{
        final RandomClusterGenerator randomClusterGenerator = new RandomClusterGenerator();

        MDG mdg = randomClusterGenerator.generate.apply(graph, new MDGImpl(), new ClusterImpl());

        Assert.assertTrue(mdg.getClusters().size() <= 6);
        Assert.assertTrue(mdg.getClusters().size() > 0);
        for(Node node : graph.getNodes()) {
            boolean found = false;
            for (Cluster cluster : mdg.getClusters()){
                if (cluster.getNodes().contains(node)) {
                    found = true;
                }
             }
            Assert.assertTrue(found);
        }
    }

    @Test
    public void testConfigureNumberOfClusters() throws Exception{
        final RandomClusterGenerator randomClusterGenerator = new RandomClusterGenerator();

        final Field field = randomClusterGenerator.getClass().getDeclaredField("configureNumberOfClusters");
        field.setAccessible(true);
        ToIntFunction<Graph> function = (ToIntFunction<Graph>) field.get(randomClusterGenerator);

        int number = function.applyAsInt(graph);

        Assert.assertTrue(number > 0);
        Assert.assertTrue(number <= graph.getNodes().size());
    }

    @Test
    public void testCreateClusters() throws Exception{
        final RandomClusterGenerator randomClusterGenerator = new RandomClusterGenerator();

        final Field field = randomClusterGenerator.getClass().getDeclaredField("createClusters");
        field.setAccessible(true);
        TriFunction<MDG, Cluster, Integer, MDG> function = (TriFunction<MDG, Cluster, Integer, MDG>) field.get(randomClusterGenerator);

        MDG result = function.apply(new MDGImpl(), new ClusterImpl(), 2);

        Assert.assertEquals(result.getClusters().size(), 2);
        for(Cluster cluster : result.getClusters()){
            Assert.assertTrue(cluster.getNodes().isEmpty());
            Assert.assertEquals(cluster.getClusterFactor(), 0, 0);
        }
    }

    @Test
    public void testFillClusters() throws Exception{
        final RandomClusterGenerator randomClusterGenerator = new RandomClusterGenerator();
        MDG mdg                                             = new MDGImpl();
        Cluster cluster                                     = new ClusterImpl();
        Cluster cluster1                                    = new ClusterImpl();

        mdg.addCluster(cluster);
        mdg.addCluster(cluster1);

        final Field field = randomClusterGenerator.getClass().getDeclaredField("fillClusters");
        field.setAccessible(true);
        BiFunction<MDG, Graph, MDG> function = (BiFunction<MDG, Graph, MDG>) field.get(randomClusterGenerator);

        MDG result = function.apply(mdg, graph);

        Iterator<Cluster> iterator = result.getClusters().iterator();
        int size                   = 0;

        while (iterator.hasNext()){
            size += iterator.next().getNodes().size();
        }
        Assert.assertEquals(graph.getNodes().size(), size);

        for(Node node : graph.getNodes()){
            Iterator<Cluster> iterator2 = result.getClusters().iterator();
            boolean found = false;
            while (iterator2.hasNext()){
                if(iterator2.next().getNodes().contains(node)){
                    found = true;
                    break;
                }
            }
            Assert.assertTrue(found);
        }
    }

}
